<?php
/**
 * @author Lena Molchanova
 */

namespace Framework;

use Appllication\Model\Message;

/**
 * Controller class
 */
class Controller
{
    public $view;

    public function __construct ()
    {
        $this->view = new View;
    }

    /**
     * redirect
     * 
     * @param string $controller
     * @param string $action
     * @param mixed $args
     *
     */
    public function redirect($controller, $action, $args = null)
    {
        $argsUrl = '';

        if ($args) {

            if (is_array($args)) {
                $args = implode('/', $args);
            }
            $argsUrl = '/' . $args;
        }
        header('location: /'. $controller . '/' . $action . $argsUrl);      
    }
}