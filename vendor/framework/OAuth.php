<?php 
/**
 * @author Lena Molchanova
 */

namespace Framework;

/**
 * Model class
 */
class OAuth extends Controller {

    /**
     * checkUserSession
     *  
     */
    public static function VerificationCheck($route1, $route2)
    {
        $open = [
            '/',
            'default/index',
            'image/avatars',
            'post/posts',
            'post/read',
            'post/userposts',
            'post/categoryposts',
            'auth/register',
            'auth/login',
            'auth/logout',
            'auth/forgotpassword',
            'auth/reset',
            'user/profile',
            'category/categories'
        ];

        $url = strtolower($route1 . '/' . $route2);

        if (!in_array($url, $open) && !isset($_SESSION['user'])) {
            self::redirect('auth', 'login');
        }
    }
}