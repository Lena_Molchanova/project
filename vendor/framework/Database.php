<?php
/**
 * @author Lena Molchanova
 */

namespace Framework;

use PDO as PDO;

/**
 * Database class
 */
class Database
{
    private $connect;
    private static $_instance;

    private function __construct()
    {
        $this->connect = new \PDO('mysql:host=' . HOST . ';dbname=' . DB . ';charset=utf8', USER, PASSWORD);    
    }

    private function __clone(){}

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function getConnect()
    {
        return $this->connect;
    }
}