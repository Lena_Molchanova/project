<?php
/**
 * @author Lena Molchanova
 */

namespace Framework;

use Appllication\Helpers\PrepareSqlHelper;




/**
 * Model class
 */
class Model
{
    protected $connect;
    protected $tableName;

    public function __construct() 
    {
        $this->connect = Database::getInstance()->getConnect();
    }   

    /**
     * save 
     *
     * @param array $data 
     *
     * @return string $lastId
     */
    public function save($data)
    {
        $sql = PrepareSqlHelper::savePrepare($data, $this->tableName, static::$fieldsName);    

        $query = $this->connect->prepare($sql);
        $query->execute();
        $lastId = $this->connect->lastInsertId();
        
        return $lastId;
    }

    /**
     * updateById 
     *
     * @param string $id 
     * @param array $data
     */
    public function updateById($id, $data)
    {
        
        $sql = PrepareSqlHelper::updatePrepare($data, $this->tableName, static::$fieldsName);
        
        $query = $this->connect->prepare($sql);
        $query->execute([$id]);     
    }

    /**
     * deleteById 
     *
     * @param string $id 
     */
    public function deleteById($id)
    {
        $sql = "UPDATE {$this->tableName} SET
            is_delete = '1'
            WHERE id = ?";

        $query = $this->connect->prepare($sql);
        $query->execute([$id]);
    }

    /**
     * getTableName 
     *
     * @return string $this->tableName
     */
    public function getTableName()
    {
        return $this->tableName;
    }
}