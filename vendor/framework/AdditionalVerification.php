<?php 
/**
 * @author Lena Molchanova
 */

namespace Framework;

use Appllication\Helpers\NotificationHelper;

/**
 * Model class
 */
class AdditionalVerification extends Controller 
{
            static $rules = [
            'user/users'     => ['Admin',
                                 'Editor' => ['denied'],
                                 'User'   => ['denied'],
                                 'Banned' => ['denied']],
            'user/editrole'  => ['Admin',
                                 'Editor' => ['denied'],
                                 'User'   => ['denied'],
                                 'Banned' => ['denied']],
            'comment/edit'   => ['Admin',
                                 'Editor',
                                 'User'   => ['owner'],
                                 'Banned' => ['owner']],
            'comment/delete' => ['Admin',
                                 'Editor',
                                 'User'   => ['owner'],
                                 'Banned' => ['owner']],
            'post/edit'      => ['Admin',
                                 'Editor',
                                 'User'   => ['owner'],
                                 'Banned' => ['owner']],
            'post/delete'    => ['Admin',
                                 'Editor',
                                 'User'   => ['owner'],
                                 'Banned' => ['owner']],
            'comment/add'    => ['Admin',
                                 'Editor',
                                 'User',
                                 'Banned' => ['denied']],
            'category/add'   => ['Admin',
                                 'Editor',
                                 'User',
                                 'Banned' => ['denied']],
            'post/add'       => ['Admin',
                                 'Editor',
                                 'User',
                                 'Banned' => ['denied']],
            'image/delete'   => ['Admin'  => ['owner'],
                                 'Editor' => ['owner'],
                                 'User'   => ['owner'],
                                 'Banned' => ['owner']],
            'image/apply'    => ['Admin'  => ['owner'],
                                 'Editor' => ['owner'],
                                 'User'   => ['owner'],
                                 'Banned' => ['owner']],
            'message/read'   => ['Admin'  => ['owner'],
                                 'Editor' => ['owner'],
                                 'User'   => ['owner'],
                                 'Banned' => ['owner']],
            'message/delete' => ['Admin'  => ['owner'],
                                 'Editor' => ['owner'],
                                 'User'   => ['owner'],
                                 'Banned' => ['owner']]
        ];
    /**
     * check - check access to url
     * 
     * @param array $owner
     */
    public static function check($owner = [])
    {
        $route = explode('/', $_SERVER['REQUEST_URI']);
        $checkUrl = $route[1] . '/' . $route[2];

        foreach ((array)static::$rules[$checkUrl] as $key => $value) {
            
            if ($_SESSION['user']['role'] == (string)$key) {
                
                in_array($_SESSION['user']['id'], (array)$owner)?:
                die(Header( "HTTP/1.1 403 Restricted Content" ));                   
            } 
        }   
    }   
}