<?php
/**
 * @author Lena Molchanova
 */

namespace Framework;

/**
 * Route class
 */
class Route
{
    /**
     * start 
     *
     * @todo 404
     */
    static function start() 
    {
        $url   = $_SERVER['REQUEST_URI'];
        $route = explode('/', $url);
        $args  = array_splice($route, 3);

        $defaultClass  = empty($route[1]) ? 'default': $route[1];
        $defaultAction = empty($route[2]) ? 'index'  : $route[2];

        OAuth::VerificationCheck($defaultClass, $defaultAction);

        $controllerName = "Appllication\\Controllers\\" . ucfirst($defaultClass) . 'Controller';
        $actionName     = $defaultAction . 'Action';;

        $controller = new $controllerName();

        if(method_exists($controller, $actionName)) {
            
            call_user_func_array(array ($controller, $actionName), $args);
        }

    }
    
    /**
     * ErrorPage404 
     * 
     */
    static function ErrorPage404() {

        header('HTTP/1.1 404 Not Found');
        header('Status: 404 Not Found');

    }
}