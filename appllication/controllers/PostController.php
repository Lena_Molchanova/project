<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Controllers;

use Appllication\Model\Post;
use Appllication\Model\Comment;
use Appllication\Model\Category;
use Appllication\Helpers\Validator;
use Appllication\Helpers\ImageHelper;
use Appllication\Helpers\CommentHelper;
use Appllication\Helpers\CategoryHelper;
use Appllication\Helpers\PrepareSqlHelper;
use Framework\{Controller, AdditionalVerification};

/**
 * PostController class
 */
class PostController extends Controller 
{
    /**
     * postsAction - render view with all posts
     */
    public function postsAction()
    {
        $model = new Post();
        
        $posts = CommentHelper::countCommentsPost(['posts' => $model->getAll()]);
        $posts = CategoryHelper::getCategoriesPosts(compact('posts'));

        $this->view->render('layout.php', 'post/posts.php', compact('posts'));
    }

    /**
     * editAction - update post.
     * render view
     *
     * @param string $id - post id
     */
    public function editAction($id)
    {
        $model = new Post();

        $categoryModel = new Category;
        $postCategories = CategoryHelper::getCategoryByPost($id);

        if (isset($_POST['post'])) {

            $post = $_POST['post'];
            $errors = Validator::check($post, 'Post');

            if (count($errors) > 0) {

                $categories = $categoryModel->getAll();
                
                $post = $model->getForUpdate($id);
                $this->view->render('layout.php', 'post/update_post.php', compact('errors', 'post', 'categories', 'postCategories'));

            } else {
                
                CategoryHelper::updateCategoriesByPost($id, $categories = $_POST['category'], $postCategories);
                ImageHelper::uploadImage($_FILES['postImage'], $id, 'Post');
                $model->updateById($id, $post);

                $this->redirect('post', 'read', $id);
            }           
        } else {

            $categories = $categoryModel->getAll();
            $post = $model->getForUpdate($id);

            AdditionalVerification::check([$post['user_id']]);

            $this->view->render('layout.php', 'post/update_post.php', compact('post','categories', 'postCategories'));
        }
    }

    /**
     * deleteAction - virtual delete post.
     * render view
     *
     * @param string $id - post id
     */
    public function deleteAction($id)
    {
        $model = new Post();
        $post = $model->getForUpdate($id);

        AdditionalVerification::check([$post['user_id']]);

        $model->deleteById($id);

        $this->redirect('post', 'posts');   
    }

    /**
     * addAction - add new post.
     * render view
     */
    public function addAction()
    {   
        if (isset($_POST['post'])) {                    

            $post = $_POST['post'];
            $errors = Validator::check($post, 'Post');

            if (count($errors) > 0) {

                $categoryModel = new Category;
                $categories = $categoryModel->getAll();
                
                $this->view->render('layout.php', 'post/add_post.php', compact('errors', 'post', 'categories'));

            } else {

                $model = new Post();
                $lastPostId = $model->save(PrepareSqlHelper::postSaveDataPrepare($post));

                CategoryHelper::savePostCategories($lastPostId, ['categories'=>$_POST['category']]);
                ImageHelper::uploadImage($_FILES['postImage'], $lastPostId, 'Post');

                $this->redirect('post', 'posts');
            }       
        } else {

            AdditionalVerification::check();

            $categoryModel = new Category;
            $categories = $categoryModel->getAll();
            
            $this->view->render('layout.php', 'post/add_post.php', compact('categories'));
        }
    }

    /**
     * readAction - render view with one post
     *
     * @param string $id - post id
     */
    public function readAction($id)
    {
        $model = new Post();

        $_SESSION['posts'] = $_SESSION['posts']??[' '];
            
        if (!in_array($id, $_SESSION['posts'])) {
        
            $_SESSION['posts'][] = $id;
            $model->plusView($id);
        }
        
        $post = $model->getById($id);
        
        $commentModel = new Comment();
        $comments = $commentModel->getAllByPost($id);

        $categories = CategoryHelper::getCategoryByPost($id);

        $this->view->render('layout.php', 'post/read_post.php', compact('post', 'comments', 'categories'));
    }

    /**
     * userPostsAction - render view with all posts written by the user
     *
     * @param string $id - user id
     */
    public function userPostsAction($id)
    {
        $model = new Post();
        $posts = CommentHelper::countCommentsPost(['posts' => $model->getAllbyUser($id)]);
        $posts = CategoryHelper::getCategoriesPosts(compact('posts'));
        
        $this->view->render('layout.php', 'post/posts.php', compact('posts'));
    }

    /**
     * userPostsAction - render view with all posts by selected category
     *
     * @param string $id - category id
     */
    public function categoryPostsAction($id)
    {
        $posts = CategoryHelper::getAllPostsByCategory($id);            
        $posts = CommentHelper::countCommentsPost($posts);
        $posts = CategoryHelper::getCategoriesPosts(compact('posts'));
                
        $this->view->render('layout.php', 'post/posts.php', compact('posts'));
    }
}
