<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Controllers;

use Framework\Controller;

/**
 * DefaultController class
 */
class DefaultController extends Controller
{
    /**
     * indexAction - render default view
     */
    public function indexAction()
    {
        $this->view->render('layout.php', null);
    }
}