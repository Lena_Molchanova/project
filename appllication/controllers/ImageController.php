<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Controllers;

use Appllication\Model\{User, ImageUser};
use Framework\{Controller, AdditionalVerification};

/**
 * ImageController class
 */
class ImageController extends Controller
{
    /**
     * avatarsAction - render view with all user images
     *
     * @param string $userId - user id 
     */
    public function avatarsAction($userId)
    {
        $model = new ImageUser;
        $images = $model->getAllByUser($userId);

        $modelUser = new User;
        $user = $modelUser->getAllBy('id', $userId);

        $this->view->render('layout.php','image/avatars.php', compact('user', 'images'));
    }

    /**
     * deleteAction - virtual delete image.
     * render view
     *
     * @param string $id - user image id
     */
    public function deleteAction($id)
    { 
        $model = new ImageUser;
        $userId = $model->getUserId($id);

        AdditionalVerification::check([$userId]);

        $model->deleteFromUser($id);

        $this->redirect('image', 'avatars', $_SESSION['user']['id']);
    }

    /**
     * applyAction - make the selected image the main one
     * render view
     *
     * @param string $id - user image id
     */
     public function applyAction($id)
    {
        $model = new ImageUser;
        $userId = $model->getUserId($id);

        AdditionalVerification::check([$userId]);

        $imageId = $model->getImageId($_SESSION['user']['id']);

        $model->deleteById($imageId);
        $model->applyFromUser($id);
        
        $this->redirect('image', 'avatars', $_SESSION['user']['id']);
    }


}