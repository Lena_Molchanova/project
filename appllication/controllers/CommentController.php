<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Controllers;

use Appllication\Model\Comment;
use Appllication\Helpers\Validator;
use Appllication\Helpers\PrepareSqlHelper;
use Framework\{Controller, AdditionalVerification};

/**
 * CommentController class
 */
class CommentController extends Controller
{
    /**
     * editAction - check errors and sends data to save
     * render view
     *
     * @param string $postId - post id where the comment is edited
     * @param string $id - comment id
     */
    public function editAction(string $postId, string $id)
    {
        $model = new Comment();

        if (isset($_POST['comment'])) {

            $comment = $_POST['comment'];
            $errors = Validator::check($comment, 'Comment');

            if (count($errors) > 0) {

                $comment = $model->getById($id);
                $this->view->render('layout.php', 'comment/update_comment.php', compact('errors', 'comment', 'postId'));

            } else {

                $model->updateById($id, $comment);
                $this->redirect('post', 'read', $postId);
            }       
        } else {

            $comment = $model->getById($id);

            AdditionalVerification::check([$comment['user_id']]);

            $this->view->render('layout.php', 'comment/update_comment.php', compact('comment', 'postId'));
        }
    }   

    /**
     * deleteAction - virtual delete comment
     * render view with post where the comment is delete
     *
     * @param string $postId - post id where the comment is delete
     * @param string $id - comment id
     */
    public function deleteAction($postId, $id)
    {
        $model = new Comment();
        $comment = $model->getById($id);

        AdditionalVerification::check([$comment['user_id']]);

        $comment = $model->deleteById($id);

        $this->redirect('post', 'read', $postId);
    }

    /**
     * addAction - add new comment
     * render view
     *
     * @param string $postId - post id to which the comment is written
     */
    public function addAction($postId)
    {
        if (isset($_POST['comment'])) {         

            $comment = $_POST['comment'];
            $errors = Validator::check($comment, 'Comment');

            if (count($errors) > 0) {

                $this->view->render('layout.php', 'comment/add_comment.php', compact('errors', 'comment', 'postId'));

            } else {

                $model = new Comment();
                $model->save(PrepareSqlHelper::commentSaveDataPrepare($comment, $postId));

                $this->redirect('post', 'read', $postId);
            }           
        } else{

            AdditionalVerification::check();

            $this->view->render('layout.php', 'comment/add_comment.php', compact('postId'));
        }
    }
}
