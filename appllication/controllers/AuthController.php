<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Controllers;

use Framework\Controller;
use Appllication\Model\User;
use Appllication\Helpers\Validator;
use Appllication\Helpers\MailHelper;
use Appllication\Model\PassRecovery;
use Appllication\Helpers\ImageHelper;
use Appllication\Helpers\NotificationHelper;

/**
 * AuthController class
 */
class AuthController extends Controller 
{
    /**
     * registerAction - register new user.
     * render view
     */
    public function registerAction()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $userData = $_POST['user'];
            $errors = Validator::check($userData, 'User');

            if (count($errors) > 0) {

                $this->view->render('layout.php', 'auth/registration_form.php', compact('errors', 'userData'));

            } else {

                $userData['password'] = md5(trim($userData['password']));

                $model = new User;                
                $lastUserId = $model->save($userData);

                ImageHelper::uploadImage($_FILES['userImage'], $lastUserId, 'User');
                $this->redirect('Auth', 'login');
            }

        } else {
            $this->view->render('layout.php', 'auth/registration_form.php');
        }
    }

    /**
     * loginAction - login user.
     * render view
     */
    public function loginAction()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            
            $userData = $_POST['user'];
            $userData['password'] = md5(trim($userData['password']));

            $errors = Validator::check($userData, 'User', 'login');

            if (count($errors) > 0) {

                $this->view->render('layout.php', 'auth/login_form.php', compact('errors', 'userData'));

            } else {

                $model = new User;
                $user = $model->getAllBy('login', $userData['login']);

                $_SESSION['user'] = [
                    'first_name' => $user['first_name'],
                    'last_name'  => $user['last_name'],
                    'messages'   => $user['messages'],
                    'role_id'    => $user['role_id'],
                    'role'       => $user['role'],
                    'login'      => $user['login'],
                    'id'         => $user['id']
                ];

                $_SESSION['posts'] = [' '];
               
                $this->redirect('default', 'index');
            }

        } else {
            $this->view->render('layout.php', 'auth/login_form.php');
        }
    }

    /**
     * logoutAction - logout user.
     * render view
     */
    public function logoutAction()
    {
        unset($_SESSION['user']);
        $this->redirect('default', 'index');
    }

    /**
     * forgotPasswordAction - send mail with link for reset password.
     * render view
     */
    public function forgotPasswordAction()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $userData = $_POST['user'];            
            $errors = Validator::check($userData, 'User', 'forgotPassword');

            if (count($errors) > 0) {

                $this->view->render('layout.php', 'auth/forgotPassword_form.php', compact('errors'));

            } else {

                $model = new User;
                $user = $model->getAllBy('email', $userData['email']);

                MailHelper::resetPasswordRequest($user);
                $this->view->render('/notification.php', null, ['notice' => NotificationHelper::$infoText[0]]);
            }

        } else {

            $this->view->render('layout.php', 'auth/forgotPassword_form.php');
        }
    }

    /**
     * resetAction - update password
     * render view
     *
     * @param string $accessHash - unique link from mail 
     */
    public function resetAction($accessHash)
    {
        $model = new PassRecovery();
        $passRecovery = $model->getAllBy('access_hash', $accessHash);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $userData = $_POST['user'];
            $errors = Validator::check($userData, 'User');

            if (count($errors) > 0) {

                $this->view->render('layout.php','auth/resetPassword_form.php', compact('errors', 'passRecovery'));

            } else {

                $userData['password'] = md5(trim($userData['password']));

                $model = new User;
                $model->updateById($passRecovery['user_id'], $userData);

                $this->view->render('/notification.php', null, ['notice' => NotificationHelper::$infoText[2]]);
            }
        } else {            

            if (date('Y-m-d H:i:s') > $passRecovery['expires']) {

                $this->view->render('/notification.php', null, ['notice' => NotificationHelper::$infoText[1]]);

            } else {

                $this->view->render('layout.php', 'auth/resetPassword_form.php', compact('passRecovery'));
            }
        }        
    }
}