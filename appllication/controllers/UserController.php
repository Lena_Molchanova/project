<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Controllers;

use Appllication\Model\User;
use Appllication\Helpers\Validator;
use Appllication\Helpers\ImageHelper;
use Appllication\Helpers\NotificationHelper;
use Framework\{Controller, AdditionalVerification};

/**
 * UserController class
 */
class UserController extends Controller
{
    /**
     * usersAction - render view with all users
     */
    public function usersAction()
    {
        AdditionalVerification::check();

        $model = new User();

        $users = $model->getAllUsers();

        $this->view->render('layout.php', 'user/users.php', compact('users'));
    }

    /**
     * updateAction - update user profile
     * render view
     */
    public function updateAction()
    {
        $model = new User;        

        if ($_SERVER['REQUEST_METHOD'] == 'POST') { 

            $userData = $_POST['user'];
            $errors = Validator::check($userData, 'User');

            if (count($errors) > 0) {

                $user = $model->getProfileBy($_SESSION['user']['id']);
                $this->view->render('layout.php', 'user/updateProfile/update_profile.php', compact('errors', 'user', 'userData'));

            } else {
                
                $model->updateById($_SESSION['user']['id'], $userData);

                ImageHelper::uploadImage($_FILES['userImage'], $_SESSION['user']['id'], 'User');

                $this->view->render('/notification.php', null, ['notice' => NotificationHelper::$infoText[3]]);
            }
            
        } else {

            $user = $model->getProfileBy($_SESSION['user']['id']);

            $this->view->render('layout.php', 'user/updateProfile/update_profile.php', compact('user'));
        }
    }

    /**
     * updateAction - update user important setting (password, email, login)
     * render view
     */
    public function settingsAction()
    {
        $model = new User;

        if ($_SERVER['REQUEST_METHOD'] == 'POST') { 

            $userData = $_POST['user'] ?? $_POST['pass_user'];           

            $userData['password'] = md5(trim($userData['password']));
            $errors = Validator::check($userData, 'User', 'updateSettings');

            if (count($errors) > 0) {

                $user = $model->getProfileBy($_SESSION['user']['id']);
                $this->view->render('layout.php', 'user/updateProfile/update_settings.php', 
                                     compact('errors', 'user', 'type'));

            } else {

                if($userData['new_password']) {
                    $userData['password'] = md5(trim($userData['new_password']));
                }
                
                $model->updateById($_SESSION['user']['id'], $userData);
                $this->view->render('/notification.php', null, ['notice' => NotificationHelper::$infoText[3]]);
            }
            
        } else {

            $user = $model->getProfileBy($_SESSION['user']['id']);            
            $this->view->render('layout.php', 'user/updateProfile/update_settings.php',  compact('user'));

        }
    }

    /**
     * profileAction - render view with user profile
     *
     * @param string $id - user id
     */
    public function profileAction($id)
    {
        $model = new User;

        $user = $model->getProfileBy($id);

        $this->view->render('layout.php','user/profile.php', compact('user'));
    }

    /**
     * editRoleAction - edit users role
     * render view
     *
     * @param string $userId - user id
     * @param string $roleId - role id
     */
    public function editRoleAction($userId, $roleId)
    {
        AdditionalVerification::check();
        
        $model = new User;
        $data = ['role_id' => $roleId];

        $model->updateById($userId, $data);

        $this->redirect('User', 'users');
    }

    
}
