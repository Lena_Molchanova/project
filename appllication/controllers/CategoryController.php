<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Controllers;

use Framework\Controller;
use Appllication\Model\Message;
use Appllication\Model\Category;
use Appllication\Helpers\Validator;
use Framework\AdditionalVerification;
use Appllication\Helpers\PrepareSqlHelper;

/**
 * CategoryController class
 */
class CategoryController extends Controller 
{
    /**
     * AddAction - check errors and sends data to save
     * render view
     */
    public function addAction()
    {
        if (isset($_POST['category'])) {
            $model = new Category;
            $categories = $model->getAll();

            $category = $_POST['category'];

            $errors = Validator::check($category, 'Category');

            if (count($errors) > 0) {               
                    
                $this->view->render('layout.php', 'category/add_category.php', compact('errors', 'categories'));

            } else {                

                $model->save(PrepareSqlHelper::categorySaveDataPrepare($category));

                $this->redirect ('category', 'categories');
            }
        } else {
            AdditionalVerification::check($comment);

            $this->view->render('layout.php', 'category/add_category.php');
        }
    }

    /**
     * categoriesAction - render view with all categories
     */
    public function categoriesAction()
    {
        $model = new Category;
        $categories = $model->getAll();

        $this->view->render('layout.php', 'category/categories.php', compact('categories'));
    }
}