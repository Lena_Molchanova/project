<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Controllers;

use Appllication\Model\Message;
use Appllication\Helpers\Validator;
use Appllication\Helpers\PrepareSqlHelper;
use Framework\{Controller, AdditionalVerification};

/**
 * MessageController class
 */
class MessageController extends Controller
{
    /**
     * writeAction - check errors and sends data to save
     * render view
     *
     * @param string $toUserId
     */
    public function writeAction($toUserId)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $message = $_POST['message'];
            $errors = Validator::check($message, 'Message');

            if (count($errors) > 0) {

                $this->view->render('layout.php', 'message/message_form.php', compact('errors', 'message', 'toUserId'));

            } else {

                $model = new Message();
                $model->save(PrepareSqlHelper::messageSaveDataPrepare($message, $toUserId));

                $this->redirect('message', 'outbox');
            }           
        } else {
            $this->view->render('layout.php', 'message/message_form.php', compact('toUserId'));
        }
    }

    /**
     * inboxAction - render view list of incoming messages 
     */
    public function inboxAction()
    {
        $messageModel = new Message;

        $messages = $messageModel->getAllInbox();

        $this->view->render('layout.php','message/inbox.php', compact('messages'));
    }

    /**
     * outboxAction - render view list of outgoing messages 
     */
    public function outboxAction()
    {
        $messageModel = new Message;

        $messages = $messageModel->getAllOutbox();

        $this->view->render('layout.php','message/outbox.php', compact('messages'));
    }

    /**
     * readAction - render view message
     *
     * @param string $id - message id
     * @param string $isRead - mark is it read
     *
     * @return array $message
     */
    public function readAction($id)
    {
        $messageModel = new Message;

        $message = $messageModel->getBy($id);
        
        AdditionalVerification::check([$message['user_to'],$message['user_from']]);

        if ($message['user_to'] == $_SESSION['user']['id']) {
            
            unset($_SESSION['user']['messages']);
            $messageModel->markIsRead($id);
        }   

        $this->view->render('layout.php','message/read_message.php', compact('message'));       
    }

    /**
     * deleteAction - virtual delete message
     * render view
     *
     * @param string $id - message id
     */
    public function deleteAction($id)
    {
        $messageModel = new Message;

        $usersID = $messageModel->getUsersIdBy($id);

        AdditionalVerification::check($usersID);
        
        if ($usersID['user_from'] == $_SESSION['user']['id']) {
            
            $messageModel->deleteFromTo($id, 'is_delete_from');

            $this->redirect('message', 'outbox');  

        } else {
            
            $messageModel->deleteFromTo($id, 'is_delete_to');

            $this->redirect('message', 'inbox');
        }                
    }
}