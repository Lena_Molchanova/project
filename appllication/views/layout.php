<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <link rel="stylesheet" href="/web/css/main.css">

  <title>Document</title>
</head>
<body>
<div class="container">
  <div class="row">
  <br>

    <nav class="navbar navbar-default" role="navigation">
      <div class="container-fluid">
         <div class="navbar-header">
          <a class="navbar-brand" href="">Blog</a>
        </div><!--/.navbar-header -->
        <div class="collapse navbar-collapse" id="navbar-admin">
         <!-- -------------------------------MENU--------------------------------- -->

          <ul class="nav navbar-nav">
            <li class="navig"><a  href="/post/posts"><span class="glyphicon glyphicon-bullhorn"></span> Posts</a></li>
            <li class="navig"><a href="/category/categories"><span class="glyphicon glyphicon-th-large"></span> Categories</a></li>
            <?php if(isset($_SESSION['user']['role_id']) && $_SESSION['user']['role_id'] == 1):?>
            <li class="navig"><a href="/user/users"><span class="glyphicon glyphicon-user"></span> Users</a></li>
            <?php endif;?>
          </ul>
          
        <!---------------------------------LOGIN - SIGNUP------------------------------- -->
        
          <ul class="nav navbar-nav navbar-right">
            
            <?php if(isset($_SESSION['user'])):?>
              <?php if($_SESSION['user']['messages'] > 0):?>
                <li>  
                  <a href="/message/inbox" class="link-button-color" style="color: red;"><span class="glyphicon glyphicon-envelope"></span> <?=$_SESSION['user']['messages'];?> </a>
                </li> 
              <?php endif;?> 

            <li class="hidden-sm hidden-xs">                      
              <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?=$_SESSION['user']['login'];?> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="/message/inbox">Messages</a></li>
                  <li><a href="/user/profile/<?=$_SESSION['user']['id'];?>">Profile</a></li>
                  <li><a href="/user/update">Update Profile</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="/auth/logout">Log Out</a></li>
                </ul>
              </div>
            </li>
                
            <?php else:?>
              <li class="hidden-sm hidden-xs">
              <button type="button" class="btn btn-success navbar-btn">
                <a href="/auth/login" class="link-button-color"><span class="glyphicon glyphicon-lock"></span>Log In</a>                  
              </button>
              <button type="button" class="btn btn-danger navbar-btn">
                <a href="/auth/register" class="link-button-color"><span class="glyphicon glyphicon-pencil"></span> Sign Up</a>
              </button>                            
            </li>
            <?php endif; ?>
          </ul>
          
        <!---------------------------------SEARCH------------------------------- -->
        <!-- <form class="navbar-form navbar-left" role="search">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
          </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form> -->
       </div><!--/.container-fluid -->
    </nav><!--/.navbar -->
  </div>
</div>

<?php if($content_page):?>

  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <?php require_once $content_page; ?>
      </div>
    </div>
  </div>
<?php else:?>

  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <?php if (isset($_SESSION['user'])) : ?>
          <h1 class="text-center">Welcome, <?=$_SESSION['user']['first_name'];?></h1>
          <a href="It's so nice to see you!"></a>
        <?php else: ?>
          <h1 class="text-center">Welcome, Stranger!</h1>
          <p class="text-center">Come in, please!</p>
        <?php endif; ?>
      </div>
    </div>
  </div>

<?php endif;?>

</body>
</html>
