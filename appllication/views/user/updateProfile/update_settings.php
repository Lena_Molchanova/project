
<div class="container register-from">

  <div class="row main">
    <div class="panel-heading">
      <div class="panel-title text-center">
        <h1 class="title">Update Profile</h1>
        <p><small></small></p>
        <hr />
      </div>

<!-- ********** UPDATE IMPORTANT SETTINGS ********** -->

    <ul class="nav nav-tabs">
        <li role="presentation"><a href="/user/update">Profile</a></li>
        <li role="presentation" class="active"><a href="/user/settings">Settings</a></li>
      </ul>
    </div>

    <?php if(isset($errors)): ?>
      <div class="errors">
        <?php foreach ($errors as $error) : ?>
          <div class="alert alert-danger">
            <?=$error?>
          </div>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>
    
    <div class="main-login main-center">

      <!-- *** UPDATE LOGIN *** -->

      <form class="" method="post" action="/user/settings">

        <div class="form-group">
          <label for="password" class="cols-sm-2 control-label">Password</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
              <input type="password" class="form-control" name="user[password]" id="password"  placeholder="Enter your Password"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="login" class="cols-sm-2 control-label">New Login</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
              <input type="text" class="form-control" name="user[login]" id="login" value="<?=$user['login'];?>" placeholder="Enter your new Login"/>
            </div>
          </div>
        </div>
        <div class="form-group ">
          <input type="submit" class="btn btn-primary btn-lg btn-block login-button"  value="Update Login"/ >
        </div>
      </form>

      <!-- *** UPDATE EMAIL *** -->
      <hr>
      <form class="" method="post" action="/user/settings">

        <div class="form-group">
          <label for="password" class="cols-sm-2 control-label">Password</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
              <input type="password" class="form-control" name="user[password]" id="password"  placeholder="Enter your Password"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="email" class="cols-sm-2 control-label">New Email</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
              <input type="text" class="form-control" name="user[email]" id="email" value="<?=$user['email'];?>" placeholder="Enter your new Email"/>
            </div>
          </div>
        </div>
        <div class="form-group ">
          <input type="submit" class="btn btn-primary btn-lg btn-block login-button"  value="Update Email"/ >
        </div>
      </form>

      <!-- *** UPDATE PASSWORD *** -->
      <hr>
      <form class="" method="post" action="/user/settings">
        <div class="form-group">
          <label for="current" class="cols-sm-2 control-label">Current Password</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
              <input type="password" class="form-control" name="pass_user[password]" id="current"  placeholder="Enter your current Password"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="password" class="cols-sm-2 control-label">New Password</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
              <input type="password" class="form-control" name="pass_user[new_password]" id="password"  placeholder="Enter your new Password"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="confirm" class="cols-sm-2 control-label">Confirm  New Password</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
              <input type="password" class="form-control" name="pass_user[confirm_password]" id="confirm"  placeholder="Confirm your new Password"/>
            </div>
          </div>
        </div>

        <div class="form-group ">
          <input type="submit" class="btn btn-primary btn-lg btn-block login-button"  value="Update Password"/ >
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="assets/js/bootstrap.js"></script>
