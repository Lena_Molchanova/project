
<div class="container register-from">

  <div class="row main">
    <div class="panel-heading">
      <div class="panel-title text-center">
        <h1 class="title">Update Profile</h1>
        <p><small></small></p>
        <hr />
      </div>

<!-- ********** UPDATE PROFILE ONLY ********** -->

      <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="/user/update">Profile</a></li>
        <li role="presentation"><a href="/user/settings">Settings</a></li>
      </ul>      
    </div>

    <?php if(isset($errors)): ?>
      <div class="errors">
        <?php foreach ($errors as $error) : ?>
          <div class="alert alert-danger">
            <?=$error?>
          </div>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>

    <div class="main-login main-center">
      <form enctype="multipart/form-data" class="" method="post" action="/user/update">

        <div class="form-group">
          <label>Your avatar</label>
          <img class="img-responsive" src="<?=$user['url'];?>" />
          <input name="userImage" type="file" accept="image/*,image/jpeg">
        </div>
        
        <div class="form-group">
          <label for="name" class="cols-sm-2 control-label">Your First Name</label>
          <p><small>Must be filled out</small></p>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
              <input type="text" class="form-control" name="user[first_name]" id="name" value="<?=$userData['first_name']??$user['first_name'];?>" placeholder="Enter your First Name"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="name" class="cols-sm-2 control-label">Your Last Name</label>
          <p><small>Must be filled out</small></p>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
              <input type="text" class="form-control" name="user[last_name]" id="name" value="<?=$userData['last_name']??$user['last_name'];?>" placeholder="Enter your Last Name"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="birthday" class="cols-sm-2 control-label">Your Birthday</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
              <input type="date" class="form-control" name="user[birthday]" id="birthday" value="<?=$userData['birthday']??$user['birthday'];?>"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="phone" class="cols-sm-2 control-label">Your Phone</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
              <input type="tel" class="form-control" name="user[phone]" id="phone" value="<?=$userData['phone']??$user['phone'];?>" placeholder="Enter your phone"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="country" class="cols-sm-2 control-label">Your Country</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
              <input type="text" class="form-control" name="user[country]" id="country" value="<?=$userData['country']??$user['country'];?>" placeholder="Enter your country"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="about" class="cols-sm-2 control-label">About You</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
              <textarea class="form-control" name="user[about]" id="about" placeholder="Tell us about you" rows="12"><?=$userData['about']??$user['about'];?></textarea>
            </div>
          </div>
        </div>

        <div class="form-group ">
          <input type="submit" class="btn btn-primary btn-lg btn-block login-button"  value="Update Profile"/ >
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="assets/js/bootstrap.js"></script>
