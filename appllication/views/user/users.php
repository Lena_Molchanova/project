<h1>Users</h1>


<table class="table">

  <thead>
    <tr>
      <th width="5">Id</th>
      <th>Name</th>
      <th>Write Message</th>      
      <th>Role</th>
    </tr>
  </thead>

  <tbody>
    <?php foreach ($users as $user) : ?>

      <tr>
        <td><?=$user['id']?></td>
        <td><a href="/user/profile/<?=$user['id'];?>"><?=$user['first_name']?> <?=$user['last_name']?></a></td> 
        <td><a href="/user/profile/<?=$user['id'];?>"><i class="glyphicon glyphicon-envelope"></i></a></td> 
        <td>
          <div class="dropdown">

            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width: 80px">
            <?=$user['role_name']?>
            <span class="caret"></span>
            </button>

            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a href="/user/editrole/<?=$user['id']?>/1">Admin</a></li>
            <li><a href="/user/editrole/<?=$user['id']?>/2">Editor</a></li>
            <li><a href="/user/editrole/<?=$user['id']?>/3">User</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="/user/editrole/<?=$user['id']?>/4">Ban</a></li>
            </ul>

          </div>
        </td>   
      </tr>

    <?php endforeach; ?>
  </tbody>
</table>