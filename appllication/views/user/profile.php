<div class="container">
  <div class="row">    
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
      <div class="panel panel-info">

        <div class="panel-heading">
          <h3 class="panel-title"><?=$user['first_name'];?> <?=$user['last_name'];?></h3>
          <?php if($user['role_id']==1 || $user['role_id']==2):?>                        
              <h3 class="pull-right"><?=$user['role_name'];?></h3>                        
          <?php endif;?>
          <?php if($user['role_id']==4):?>                        
              <h3 class="pull-right" style="color: red"><?=$user['role_name'];?></h3>                        
          <?php endif;?>
        </div>

        <div class="panel-body">
          <div class="row">

          <?php if($user['url']):?>
            <a href="/image/avatars/<?=$user['id'];?>" title="Show all avatars">
              <div class="col-md-3 col-lg-3 " align="center"> <img alt="See all avatars" src="<?=$user['url'];?>" class="img-circle img-responsive"> All avatars
              </div>
            </a>
          <? else :?>
            <div class="col-md-3 col-lg-3 " align="center"> <img alt="See all avatars" src="\web\uploadFiles\image\user\user-default.png" class="img-circle img-responsive">
            </div>
          <? endif;?>

            <div class=" col-md-9 col-lg-9 "> 
              <table class="table table-user-information">
                <tbody>

                  <?php if ($user['country']):?>
                    <tr>
                      <td>Country:</td>
                      <td><?=$user['country'];?></td>
                    </tr>
                  <?php endif;?>
                  
                  <tr>
                    <td>Join date:</td>
                    <td><?=date('d-m-Y', strtotime($user['created_at']));?></td>
                  </tr>

                  <?php if ($user['birthday']):?>
                    <tr>
                      <td>Birth Date:</td>
                      <td><?=$user['birthday'];?></td>
                    </tr>
                  <?php endif;?>


                  <?php if ($user['phone']):?>
                    <tr>
                      <td>Phone:</td>
                      <td><?=$user['phone'];?></td>
                    </tr>
                  <?php endif;?>

                  <?php if ($user['about']):?>
                    <tr>
                      <td>About:</td>
                      <td><?=$user['about'];?></td>
                    </tr>
                  <?php endif;?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        
        <div class="panel-footer">
          <?php if($user['id'] == $_SESSION['user']['id']):?>
          <a href="/message/inbox" data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary" title="Show inbox">
          <i class="glyphicon glyphicon-envelope"></i></a>
          
          <a href="/user/update" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit" title="Edit profile"></i></a>
          
          <?php else :?>
          <a href="/message/write/<?=$user['id'];?>" data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary" title="Write message"><i class="glyphicon glyphicon-envelope"></i></a>
          <?php endif;?> 
          
          <?php if($user['posts'] > 0):?>
          <span class="pull-right">
            <a href="/post/userposts/<?=$user['id'];?>" class="btn btn-primary">Read all posts by <?=$user['first_name'];?> <?=$user['last_name'];?> (<?=$user['posts'];?>)</a>
          </span>
          <?php endif;?>

        </div>
    
      </div>
    </div>
  </div>
</div>
