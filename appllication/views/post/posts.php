
<h1>Posts</h1>
<?php if(isset($_SESSION['user'])
     && $_SESSION['user']['role_id'] != 4):?>
<a class="btn btn-default" href="/post/add">Add post</a>
<br><br>
<?php endif;?>

<div class="container">
  <div class="row">
  <?php foreach ($posts as $post) : ?>
    <div class="row"> 

      <div class="col-xs-12 col-sm-3 col-md-3">                    
        <a href="/post/read/<?=$post['id'];?>" title="Read post">
          <img src="<?=$post['url']??'\web\uploadFiles\image\post\no-default-thumbnail.png';?>" class="img-responsive img-box img-thumbnail"> 
        </a>
      </div>

      <div class="col-xs-12 col-sm-9 col-md-9">
        <div class="list-group">
          <div class="list-group-item">
            <div class="row-content">

              <div class="list-group-item-heading">
                <a href="/user/profile/<?=$post['user_id'];?>" title=" Show profile <?=$post['first_name'];?> <?=$post['last_name'];?>">
                  <small><?=$post['first_name'] . " " . $post['last_name'];?></small>
                </a>
              </div>

              <small>
                <i class="glyphicon glyphicon-time"></i> <?=date('d-m-Y', strtotime($post['created_at']));?> 
                <br>
                <span><i>Views: <?=$post['views'];?></i></span><br>
                <span><i>Comments: <?=$post['comments'];?></i></span>
                <br>
                <?php if ($post['category']):?>
                <i class="glyphicon glyphicon-th-large"></i>
                  <?php foreach ($post['category'] as $key => $category):?>
                    <?php foreach($category as $id => $name):?>
                    <b><a href="/post/categoryposts/<?=$id;?>" title="Show all posts"><?=$name;?></a></b>
                    <?php endforeach;?>
                  <?php endforeach;?>
                <?php endif;?>
              </small>

            </div>
          </div>
        </div>

        <h4><a href="/post/read/<?=$post['id'];?>" title="Read the post"><?=$post['title'];?></a></h4>
        <p><?=$post['description'];?></p>
        
      </div> 
    </div>
    <hr>
  <?php endforeach; ?>
  </div>
</div>

