
<div class="container">
  <div class="well"> 
    <div class="row">
       <div class="col-md-12">

         <div class="row hidden-md hidden-lg"><h1 class="text-center" ><?=$post['title'];?></h1></div>

         
           <?php if ($categories):?>
          <i class="glyphicon glyphicon-th-large"></i>
            <?php foreach ($categories as $category):?>
              <?php foreach($category as $id => $name):?>
              <b><a href="/post/categoryposts/<?=$id;?>" title="Show all posts"><?=$name;?></a></b>
              <?php endforeach;?>
            <?php endforeach;?>
          <?php endif;?>
         
           
         <div class="pull-left col-md-6 col-xs-12 thumb-contenido"><img class="center-block img-responsive" src="<?=$post['url']??'\web\uploadFiles\image\post\no-default-thumbnail.png';?>" alt="<?=$post['title'];?>" />
         </div>

         <div class="">
           <h1  class="hidden-xs hidden-sm"><?=$post['title'];?></h1>

           <hr>

           <small>Published: <?=date('d-m-Y', strtotime($post['created_at']))?></small><br>
           <?php if(isset($post['updated_at'])):?>
           <small>Update: <?=$post['updated_at'];?></small><br>
           <?php endif; ?>
           <small><strong><a href="/user/profile/<?=$post['user_id'];?>" title="Show profile">
            <?=$post['first_name'];?> <?=$post['last_name'];?>
           </a></strong></small>
           <hr>
           <p class="text-justify"><?=$post['content'];?></p>
         </div>

         <?php if($post['user_id'] == $_SESSION['user']['id'] 
            || $_SESSION['user']['role_id'] == 1 || $_SESSION['user']['role_id'] == 2):?>
         <ul class="nav nav-pills">
          <li role_id="presentation"><a href="/post/edit/<?=$post['id'];?>">edit</a></li>
          <li role_id="presentation"><a href="/post/delete/<?=$post['id'];?>">delete</a></li> 
          <?php endif;?>   
        </ul>
       </div>
    </div>
  </div>
</div>

<?php if(!isset($_SESSION['user'])):?>
<p class="text-right">
  <a href="/auth/login" class="btn btn-default">Write comment</a>
</p>
<?php elseif($_SESSION['user']['role_id'] != 4):?>
<p class="text-right">
  <a href="/comment/add/<?=$post['id'];?>" class="btn btn-default">Write comment</a>
</p>
<?php endif;?>

<br>

<?php if(isset($comments[0])) :?>
<div class="container">
  <div class="row">
  <div class="col-md-12">
    <h2 class="page-header">Comments</h2>

    <?php foreach ($comments as $comment) : ?>
    <section class="comment-list">
      <article class="row">
      <div class="col-md-2 col-sm-2 hidden-xs">

        <figure class="thumbnail">
        <img class="img-responsive" src="<?=$comment['url']??'\web\uploadFiles\image\user\user-default.png';?>" />
        <figcaption class="text-center">
          <a href="/user/profile/<?=$comment['user_id'];?>" title="Show profile">
          <?=$comment['first_name'];?> <?=$comment['last_name'];?>
          </a>
        </figcaption>
        </figure>

      </div>
      <div class="col-md-10 col-sm-10">
        <div class="panel panel-default arrow left">
        <div class="panel-body">

          <header class="text-left">
          <div class="comment-user"><strong><?=$comment['title'];?></strong></div>
          <time class="comment-date" datetime="<?=$comment['created_at'];?>"><i class="fa fa-clock-o"><small><?=date('d-m-Y', strtotime($comment['created_at']));?></small></i></time>
          </header>

          <div class="comment-post">
          <p>
            <?=$comment['content'];?>
          </p>
          </div>

          <?php if($comment['user_id'] == $_SESSION['user']['id']
              || $_SESSION['user']['role_id'] == 1 || $_SESSION['user']['role_id'] == 2):?>
          <p class="text-right"><a href="/comment/edit/<?=$post['id'];?>/<?=$comment['id'];?>" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> edit</a>
          <a href="/comment/delete/<?=$post['id'];?>/<?=$comment['id'];?>" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> delete</a></p>
         <?php endif;?>
         
        </div>
        </div>
      </div>
    </section>
    <?php endforeach; ?>
  </div>
  </div>
</div>
<?php endif; ?>
