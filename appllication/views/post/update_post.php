<h1 class="title">Update Post</h1>
<hr />
<?php if(isset($errors)): ?>
    <div class="errors">
        <?php foreach ($errors as $error) : ?>
            <div class="alert alert-danger">
                <?=$error?>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<form enctype="multipart/form-data" method="POST" action="/post/edit/<?=$post['id']?>" style="width: 50%">

  <div class="form-group">
    <label>Image</label>
    <img class="img-responsive" src="<?=$post['url'];?>"/>
    <input name="postImage" type="file" accept="image/*,image/jpeg" value="<?=$post['url']?>">
  </div>

	<div class="form-group">
    <label>Title</label>
    <input type="text" class="form-control" name="post[title]" value="<?=$post['title']?>" placeholder="Title">
	</div>

	<div class="form-group">
    <label>Description</label>
    <textarea class="form-control" maxlength="300" name="post[description]" placeholder="Text" rows="5"><?=$post['description']?></textarea>
	</div>

	<div class="form-group">
    <label>Content</label>
    <textarea class="form-control" name="post[content]" placeholder="Text" rows="15"><?=$post['content']?></textarea>
	</div>

    <div class="form-group">
    <label>Categories</label><br>
      <div class="col-lg-6">
        <div class="input-group">
          <span class="input-group-addon">

            <?php foreach ($categories as $category):?>
             <input type="checkbox" name="category[<?=$category['id'];?>]" value="<?=$category['id'];?>" 
             <?php for ($i=0; $i < count($postCategories); $i++):?> 
                 <?php if(array_intersect($category, $postCategories[$i])):?> checked <?php endif; endfor?> ><?=$category['name'];?>
            <?php endforeach;?>
            
          </span>
        </div><!-- /input-group -->
      </div><!-- /.col-lg-6 -->
    </div><!-- /.row --> <br>
      
 	 <button type="submit" class="btn btn-default">Submit</button>
</form> 
