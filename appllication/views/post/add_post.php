
<h1 class="title">Create New Post</h1>
<hr />

<?php if(isset($errors)): ?>
    <div class="errors">
        <?php foreach ($errors as $error) : ?>
            <div class="alert alert-danger">
                <?=$error?>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
<form enctype="multipart/form-data" method="POST" action="/post/add" style="width: 50%">

	<div class="form-group">
		<label>Image</label>
		<input name="postImage" type="file" accept="image/*,image/jpeg">
	</div>

	<div class="form-group">
		<label>Title</label>
		<input type="text" class="form-control" maxlength="255" name="post[title]" placeholder="Title" value="<?=$post['title'];?>">
	</div>

	<div class="form-group">
		<label>Description</label>
		<textarea class="form-control" maxlength="500" name="post[description]" placeholder="Description" rows="5"><?=$post['description'];?></textarea>
	</div>

	<div class="form-group">
		<label>Content</label>
		<textarea class="form-control" name="post[content]" placeholder="Text" rows="15"><?=$post['content'];?></textarea>
	</div>

	<div class="form-group">
    <label>Categories</label><br>
      <div class="col-lg-6">
        <div class="input-group">
          <span class="input-group-addon">

            <?php foreach ($categories as $category):?>
             <input type="checkbox" name="category[<?=$category['id'];?>]" value="<?=$category['id'];?>"><?=$category['name'];?>
            <?php endforeach;?>
            
          </span>
        </div><!-- /input-group -->
      </div><!-- /.col-lg-6 -->
    </div><!-- /.row -->
    <br>

	<button type="submit" class="btn btn-default">Submit</button>
</form>    
