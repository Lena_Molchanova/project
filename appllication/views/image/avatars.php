
<div class="container">
  <h1 class="text-center"><?=$user['first_name'];?> <?=$user['last_name'];?></h1>
  <?php foreach ($images as $image):?>
    <div class="col-md-4 center-block">
      <div class="section-box-eleven">
        <!---->

        <figure>
          <?php if($user['id'] == $_SESSION['user']['id']):?>
            <a href="/image/delete/<?=$image['id'];?>" class="btn pull-left" title="delete the image"><i class="fa fa-search"></i>Delete</a>
            <?php if($image['is_delete'] != 0):?>
            <a href="/image/apply/<?=$image['id'];?>" class="btn pull-right" title="Aplly the image as avatar">Apply<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
            <?php endif;?>
          <?php endif;?>
           
          <a href="" class="btn pull-right">Upload: <?=date('d-m-Y', strtotime($image['created_at']));?><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>          
        </figure>

        <img class="center-block" src="<?=$image['url'];?>"/>
        
      </div>
    </div>
  <?php endforeach;?>
</div>
