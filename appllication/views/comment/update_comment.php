<?php if(isset($errors)): ?>
  <div class="errors">
    <?php foreach ($errors as $error) : ?>
      <div class="alert alert-danger">
        <?=$error?>
      </div>
    <?php endforeach; ?>
  </div>
<?php endif; ?>
<form method="POST" action="/comment/edit/<?=$postId;?>/<?=$comment['id'];?>" style="width: 50%">

  <div class="form-group">
    <label>Title</label>
    <input type="text" class="form-control" name="comment[title]" value="<?=$comment['title'];?>" placeholder="Title">
  </div>

  <div class="form-group">
    <label>Content</label>
    <textarea class="form-control" name="comment[content]" placeholder="Content" rows="10"><?=$comment['content'];?></textarea>
  </div>
  
  <button type="submit" class="btn btn-default">Submit</button>
</form>
