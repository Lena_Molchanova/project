<?php if(isset($errors)): ?>
  <div class="errors">
    <?php foreach ($errors as $error) : ?>
      <div class="alert alert-danger">
        <?=$error?>
      </div>
    <?php endforeach; ?>
  </div>
<?php endif; ?>

<form method="POST" action="/comment/add/<?=$postId;?>" style="width: 50%">

  <div class="form-group">
    <label>Title</label>
    <input type="text" class="form-control" maxlength="255" name="comment[title]" placeholder="Title" value="<?=$comment['title'];?>">
  </div>
  
  <div class="form-group">
    <label>Content</label>
    <textarea class="form-control" name="comment[content]" placeholder="Comment" rows="5"><?=$comment['content'];?></textarea>
  </div>

  <button type="submit" class="btn btn-default">Submit</button>
</form>
