<div class="container">
  <div class="row">  

    <div class="col-xs-12 toppad" >
      <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="/message/inbox">Inbox</a></li>
        <li role="presentation"><a href="/message/outbox">Outbox</a></li>
      </ul>            
    </div>

    <div class="panel-body">
      <table class="table table-inbox table-hover">
        <tbody>
        <?php foreach($messages as $message):?>

          <?php if(!$message['is_read']):?>            
          <tr class="unread">
            <td class="view-message  dont-show"><a href="/user/profile/<?=$message['user_from'];?>"><?=$message['first_name'];?> <?=$message['last_name'];?></a></td>
            <td class="view-message "><a href="/message/read/<?=$message['id'];?>"><?=$message['title'];?></a></td>
            <td class="view-message text-right"><?=$message['created_at'];?></td>
          </tr>

          <?php else :?>

          <tr class="">
            <td class="view-message  dont-show"><a href="/user/profile/<?=$message['user_from'];?>" title="Show profile"><?=$message['first_name'];?> <?=$message['last_name'];?></a></td>
            <td class="view-message "><a href="/message/read/<?=$message['id'];?>" title="Read message"><?=$message['title'];?></a></td>
            <td class="view-message  text-right"><?=$message['created_at'];?></td>
          </tr>
          <?php endif;?>

        <?php endforeach;?>
        </tbody>
      </table>            
    </div>     
  </div>
</div>
