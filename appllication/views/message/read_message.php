<div class="container">
  <div class="row">
    <div class="col-md-12">

      <div class="col-md-2 col-sm-2 hidden-xs">
        <figure class="thumbnail">
          <img class="img-responsive" src="<?=$message['url']??'\web\uploadFiles\image\user\user-default.png';?>" />
        </figure>
      </div>

      <div class="col-md-10 col-sm-10">
        <table class="table">

          <thead>
            <tr>
              <th width="15"></th>
              <th></th>
            </tr>
          </thead>

          <tbody>                  
            <tr>
              <td>From</td>     
              <td>
                <a href="/user/profile/<?=$message['user_from'];?>" title="Show profile"><?=$message['first_name_from'];?> <?=$message['last_name_from'];?></a>
              </td>  
            </tr>

            <tr>
              <td>To</td>     
              <td><a href="/user/profile/<?=$message['user_to'];?>" title="Show profile"><?=$message['first_name_to'];?> <?=$message['last_name_to'];?></a>
              </td>  
            </tr>

            <tr>
              <td>Sent</td>     
              <td><?=date('d-m-Y', strtotime($message['created_at']));?></td>  
            </tr>

            <tr>
              <td>Title</td>     
              <td><strong><?=$message['title'];?></strong></td>  
            </tr>

            <tr>
              <td>Message</td>     
              <td><?=$message['content'];?></td>  
            </tr>

            <tr>
              <td>
                <?php if($message['user_from'] == $_SESSION['user']['id']):?>
                  <a href="/message/outbox" class="btn btn-default btn-sm" title="Back to inbox"><i class="fa fa-reply"></i>Back</a>
                <?php else:?>
                <a href="/message/inbox" class="btn btn-default btn-sm" title="Back to inbox"><i class="fa fa-reply"></i>Back</a>
                <?php endif;?>
              </td>     
              <td>
                <p class="text-right">
                <?php if($message['user_from']!=$_SESSION['user']['id']):?>
                  <a href="/message/write/<?=$message['user_from'];?>" class="btn btn-default btn-sm" title="Write message"><i class="fa fa-reply"></i>Reply</a>
                <?php endif;?>
                  <a href="/message/delete/<?=$message['id'];?>" class="btn btn-default btn-sm" title="Delete message"><i class="fa fa-reply"></i> Delete</a>
                </p>
              </td>  
            </tr>
          </tbody>
          
        </table>              
      </div>    
    </div>
  </div>
</div>

