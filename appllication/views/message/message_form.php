<?php if(isset($errors)): ?>
  <div class="errors">
    <?php foreach ($errors as $error) : ?>
      <div class="alert alert-danger">
        <?=$error?>
      </div>
    <?php endforeach; ?>
  </div>
<?php endif; ?>

<form method="POST" action="/message/write/<?=$toUserId;?>" style="width: 50%">

  <div class="form-group">
    <label>Title</label>
    <input type="text" class="form-control" maxlength="255" name="message[title]" placeholder="Title" value="<?=$message['title'];?>">
  </div>
  
  <div class="form-group">
    <label>Message</label>
    <textarea class="form-control" name="message[content]" placeholder="Message" rows="10"><?=$message['content'];?></textarea>
  </div>

  <button type="submit" class="btn btn-default">Send</button>
</form>
