
<div class="container register-from">

  <div class="row main">
    <div class="panel-heading">
      <div class="panel-title text-center">
        <h1 class="title">Reset password</h1>
        <hr />
      </div>
    </div>
    <?php if(isset($errors)): ?>
      <div class="errors">
        <?php foreach ($errors as $error) : ?>
          <div class="alert alert-danger">
            <?=$error?>
          </div>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>

    <div class="main-login main-center">
      <form class="" method="post" action="/auth/reset/<?=$passRecovery['access_hash'];?>">

        <div class="form-group">
          <label for="password" class="cols-sm-2 control-label">Password</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
              <input type="password" class="form-control" name="user[password]" id="password"  placeholder="Enter your Password"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
              <input type="password" class="form-control" name="user[confirm_password]" id="confirm"  placeholder="Confirm your Password"/>
            </div>
          </div>
        </div>

        <div class="form-group ">
          <input type="submit" class="btn btn-primary btn-lg btn-block login-button"  value="Reset password"/ >
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="assets/js/bootstrap.js"></script>
