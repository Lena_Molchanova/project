
<div class="container register-from">

  <div class="row main">
    <div class="panel-heading">
      <div class="panel-title text-center">
        <h1 class="title">Register</h1>
        <p><small>This is a quick registration. You can expand your profile later</small></p>
        <hr />
      </div>
    </div>

    <?php if(isset($errors)): ?>
      <div class="errors">
        <?php foreach ($errors as $error) : ?>
          <div class="alert alert-danger">
            <?=$error;?>
          </div>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>

    <div class="main-login main-center">
      <form enctype="multipart/form-data" class="" method="post" action="/auth/register">
        <div class="form-group">
          <label>Picture</label>
          <input name="userImage" type="file" accept="image/*,image/jpeg">
        </div>

        <div class="form-group">
          <label for="name" class="cols-sm-2 control-label">Your First Name</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
              <input type="text" class="form-control" name="user[first_name]" id="name"  placeholder="Enter your First Name" value="<?=isset($userData['first_name']) ? $userData['first_name'] : '';?>"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="name" class="cols-sm-2 control-label">Your Last Name</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
              <input type="text" class="form-control" name="user[last_name]" id="name"  placeholder="Enter your Last Name" value="<?=isset($userData['last_name']) ? $userData['last_name'] : '';?>"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="name" class="cols-sm-2 control-label">Your Login</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
              <input type="text" class="form-control" name="user[login]" id="name"  placeholder="Enter your Login" value="<?= isset($userData['login']) ? $userData['login'] : '';?>"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="email" class="cols-sm-2 control-label">Your Email</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
              <input type="text" class="form-control" name="user[email]" id="email"  placeholder="Enter your Email" value="<?= isset($userData['email']) ? $userData['email'] : '';?>"/>
            </div>
          </div>
        </div>


        <div class="form-group">
          <label for="password" class="cols-sm-2 control-label">Password</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
              <input type="password" class="form-control" name="user[password]" id="password"  placeholder="Enter your Password"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
              <input type="password" class="form-control" name="user[confirm_password]" id="confirm"  placeholder="Confirm your Password"/>
            </div>
          </div>
        </div>

        <div class="form-group ">
          <input type="submit" class="btn btn-primary btn-lg btn-block login-button"  value="Register"/ >
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="assets/js/bootstrap.js"></script>
