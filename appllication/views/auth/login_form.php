
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <h1 class="text-center">Welcome</h1>
    </div>
    <div class="modal-body">
      <form id="login-form" action="/auth/login" method="post">
        <?php if(isset($errors)): ?>
          <div class="errors">
            <?php foreach ($errors as $error) : ?>
              <div class="alert alert-danger">
                <?=$error?>
              </div>
            <?php endforeach; ?>
          </div>
        <?php endif; ?>
        <div class="form-group">
          <input type="text" class="form-control input-lg" name="user[login]" placeholder="Login" value="<?=isset($userData['login']) ? $userData['login'] : '';?>" />
        </div>

        <div class="form-group">
          <input type="password" class="form-control input-lg" name="user[password]" placeholder="Password"/>
        </div>

        <div class="form-group">
          <input type="submit" class="btn btn-block btn-lg btn-primary" value="Login"/>
          <span class="pull-right"><a href="/auth/register">Register</a></span><span><a href="/auth/forgotpassword">Forgot Password</a></span>
        </div>
      </form>
    </div>
  </div>
 </div>

