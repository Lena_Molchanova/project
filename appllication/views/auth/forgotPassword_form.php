
<div class="container register-from">

  <div class="row main">
    <div class="panel-heading">
      <div class="panel-title text-center">
        <h1 class="title">Forgot password</h1>
        <hr />
      </div>
    </div>
    <?php if(isset($errors)): ?>
      <div class="errors">
        <?php foreach ($errors as $error) : ?>
          <div class="alert alert-danger">
            <?=$error;?>
          </div>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>
    <div class="main-login main-center">
      <form class="" method="post" action="/auth/forgotpassword">

        <div class="form-group">
          <label for="email" class="cols-sm-2 control-label">Your Email</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
              <input type="text" class="form-control" name="user[email]" id="email"  placeholder="Enter your Email"/>
            </div>
          </div>
        </div>

        <div class="form-group ">
          <input type="submit" class="btn btn-primary btn-lg btn-block login-button"  value="Send Email"/ >
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="assets/js/bootstrap.js"></script>
