<h1>Categories</h1>

<?php if(isset($_SESSION['user'])
     && $_SESSION['user']['role_id'] != 4):?>
  <a class="btn btn-default" href="/category/add">Add category</a>
  <br>
  <br>
<?php endif;?>

<table class="table">

  <thead>
    <tr>
      <th>Category</th>
    </tr>
  </thead>

  <tbody>
    <?php foreach ($categories as $category) : ?>
      <tr>
        <td><a href="/post/categoryposts/<?=$category['id'];?>" title='show posts'><?=$category['name']?> </a><small>(<?=$category['posts']?>)</small></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>