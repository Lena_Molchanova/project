<h1 class="title">New Category</h1>
<hr />
  <?php if(isset($errors)): ?>
    <div class="errors">
      <?php foreach ($errors as $error) : ?>
        <div class="alert alert-danger">
          <?=$error?>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>

<form method="POST" action="/category/add" style="width: 50%">
  <div class="form-group">
    <label>Category</label>
    <input type="text" class="form-control" maxlength="255" name="category[name]" placeholder="Name of Category">
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>