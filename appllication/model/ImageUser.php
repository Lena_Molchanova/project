<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Model;

use PDO as PDO;
use Framework\Model;

/**
 * ImagePost class
 */
class ImageUser extends Model {

    protected $tableName = 'image_user';

    protected static $fieldsName = [ 
        'user_id'=> '',
        'url'     => ''
    ];

    /**
     * getImageId 
     *
     * @param string $userId 
     *
     * @return string $imageId
     */
    public function getImageId($userId)
    {
        $sql = "SELECT 
            id
            FROM {$this->tableName}            
            WHERE is_delete = 0 AND user_id = ?
            LIMIT 1";
        
        $query = $this->connect->prepare($sql);
        $query->execute([$userId]);
        $imageId = $query->fetchColumn();

        return $imageId;
    }

    /**
     * getUserId 
     *
     * @param string $imagerId 
     *
     * @return string $userId
     */
    public function getUserId($imageId)
    {
        $sql = "SELECT 
            user_id
            FROM {$this->tableName}            
            WHERE id = ?
            LIMIT 1";
        
        $query = $this->connect->prepare($sql);
        $query->execute([$imageId]);
        $userId = $query->fetchColumn();

        return $userId;
    }

    /**
     * getAllByUser 
     *
     * @param string $id
     *
     * @return array $image
     */
    public function getAllByUser($id)
    {
        $sql = "SELECT 
            *
            FROM {$this->tableName}            
            WHERE is_delete < '2' AND user_id = ?";
        
        $query = $this->connect->prepare($sql);
        $query->execute([$id]);
        $image = $query->fetchAll(PDO::FETCH_ASSOC);
        
        return $image;
    }

    /**
     * deleteFromUser 
     *
     * @param string $id
     */
    public function deleteFromUser($id)
    {
        $sql = "UPDATE {$this->tableName} SET
            is_delete = '2'
            WHERE id = ?";

        $query = $this->connect->prepare($sql);
        $query->execute([$id]);
    }

    /**
     * applyFromUser 
     *
     * @param string $id
     */
    public function applyFromUser($id)
    {
        $sql = "UPDATE {$this->tableName} SET
            is_delete = '0'
            WHERE id = ?";

        $query = $this->connect->prepare($sql);
        $query->execute([$id]);
    }
}

