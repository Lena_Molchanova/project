<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Model;

use PDO as PDO;
use Framework\Model;

/**
 * Message class
 */
class Message extends Model 
{
    protected $tableName = 'messages';

    protected static $fieldsName = [         
        'is_delete_from' => '',
        'is_delete_to' => '',
        'user_from' => '',
        'content'   => '',
        'user_to'   => '',
        'title'     => ''
    ];

    /**
     * basicValidateRules
     * 
     * @param array $data
     *
     * @return array $rules
     */
    public static function basicValidateRules($data)
    {
        $rules = [
            'title'   => ['isntEmpty'],
            'content' => ['isntEmpty']
        ];
        return $rules;
    }

    /**
     * getAllInbox - select all incoming messages by user id
     *
     * @return array $messages
     */
    public function getAllInbox()
    {
        $userModel = new User;

        $sql = "SELECT 
            m.id,
            m.title,
            m.user_from,
            m.is_read,
            m.created_at,
            u.first_name,
            u.last_name
            FROM {$this->tableName} as m
            LEFT JOIN {$userModel->getTableName()} AS u ON m.user_from = u.id 
            WHERE m.is_delete_to = '0' AND m.user_to = ? 
            ORDER BY m.created_at DESC";

        $query = $this->connect->prepare($sql);
        $query->execute([$_SESSION['user']['id']]);

        $messages = $query->fetchAll(PDO::FETCH_ASSOC);
    
        return $messages;
    }

    /**
     * getAllOutbox - select all outgoing messages by user id
     *
     * @return array $messages
     */
    public function getAllOutbox()
    {
        $userModel = new User;

        $sql = "SELECT 
            m.id,
            m.title,
            m.user_to,
            m.is_read,
            m.created_at,
            u.first_name,
            u.last_name
            FROM {$this->tableName} as m
            LEFT JOIN {$userModel->getTableName()} AS u ON m.user_to = u.id 
            WHERE m.is_delete_from = '0' AND m.user_From = ? 
            ORDER BY m.created_at DESC";

        $query = $this->connect->prepare($sql);
        $query->execute([$_SESSION['user']['id']]);

        $messages = $query->fetchAll(PDO::FETCH_ASSOC);
    
        return $messages;
    }

    /**
     * getBy - get One by id for read
     *
     * @param string $id 
     *
     * @return array $mesage
     */
    public function getBy($id)
    {
        $userModel = new User;
        $imageModel = new ImageUser;

        $sql = "SELECT 
            m.id,
            m.title,
            m.content,
            m.user_from,
            m.user_to,
            m.created_at,
            i.url,
            u.first_name as first_name_from,
            u.last_name as last_name_from,
            t.first_name as first_name_to,
            t.last_name as last_name_to
            FROM {$this->tableName} as m 
            LEFT JOIN {$userModel->getTableName()} AS u ON m.user_from = u.id
            LEFT JOIN {$userModel->getTableName()} AS t ON m.user_to = t.id
            LEFT JOIN {$imageModel->getTableName()} AS i ON m.user_from = i.user_id AND i.is_delete = '0'
            WHERE m.id = ? LIMIT 1";

        $query = $this->connect->prepare($sql);
        $query->execute([$id]);

        $messages = $query->fetch(PDO::FETCH_ASSOC);
    
        return $messages;
    }

    /**
     * getUsersIdBy - select user from id and userto id
     *
     * @param string $id - message id
     *
     * @return array
     */
    public function getUsersIdBy($id)
    {
        $sql = "SELECT 
            user_from,
            user_to
            FROM {$this->tableName}
            WHERE id = ?
            LIMIT 1";

        $query = $this->connect->prepare($sql);
        $query->execute([$id]);

        $usersId = $query->fetch(PDO::FETCH_ASSOC);

        return $usersId;
    }

    /**
     * getCountNotReadByUser - count not read messages
     *
     * @return string $number
     */
    public function getCountNotReadByUser()
    {
        $sql = "SELECT 
            COUNT(id)
            FROM {$this->tableName}
            WHERE is_read = '0' AND user_to = ?";

        $query = $this->connect->prepare($sql);
        $query->execute([$_SESSION['user']['id']]);

        $number = $query->fetchColumn();
        
        return $number;
    }

    /**
     * markIsRead - mark Message as read
     *
     * @param string $id - message id
     */
    public function markIsRead($id)
    {
        $sql = "UPDATE {$this->tableName} SET
            is_read = '1'
            WHERE id = ?";

        $query = $this->connect->prepare($sql);
        $query->execute([$id]);
    }

    /**
     * deleteById 
     *
     * @param string $id
     * @param string $field - is_delete_from or is_delete_to 
     */
    public function deleteFromTo($id, $field)
    {
        $sql = "UPDATE {$this->tableName} SET
            {$field} = '1'
            WHERE id = ?";

        $query = $this->connect->prepare($sql);
        $query->execute([$id]);
    }
}