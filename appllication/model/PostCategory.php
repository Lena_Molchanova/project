<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Model;

use PDO as PDO;
use Framework\Model;

/**
 * Category class
 */
class PostCategory extends Model
{
    protected $tableName = 'post_categories';

    protected static $fieldsName = [         
        'category_id' => '',
        'post_id'     => ''
    ];

    /**
     * getCategoryIdByPost
     * 
     * @param string $id
     *
     * @return array $categoriesId
     */
    public function getCategoriesIdByPost($id)
    {
        $sql = "SELECT category_id FROM {$this->tableName} WHERE post_id = ?";
        
        $query = $this->connect->prepare($sql);
        $query->execute([$id]);
        
        $categoriesId = $query->fetchAll(PDO::FETCH_ASSOC);

        return $categoriesId;
    }

    /**
     * getPostIdByCategory
     * 
     * @param string $id
     *
     * @return array $postsId
     */
    public function getPostsIdByCategory($id)
    {
        $sql = "SELECT post_id FROM {$this->tableName} WHERE category_id = ?";
        
        $query = $this->connect->prepare($sql);
        $query->execute([$id]);
        
        $postsId = $query->fetchAll(PDO::FETCH_ASSOC);

        return $postsId;
    }

    /** 
     * realDeleteBy - delete from DB by id
     *
     * @param string $categoryId
     * @param string $postId
     */
    public function realDeleteBy($categoryId, $postId)
    {
        $sql = "DELETE FROM {$this->tableName} WHERE category_id = ? AND post_id = ?";

        $query = $this->connect->prepare($sql);
        $query->execute([$categoryId, $postId]);
    }
}