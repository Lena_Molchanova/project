<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Model;

use PDO as PDO;
use Framework\Model;

/**
 * ImagePost class
 */
class ImagePost extends Model {

    protected $tableName = 'image_post';

    protected static $fieldsName = [  
        'post_id' => '',
        'url'     => ''
    ];

    /**
     * getImageId 
     *
     * @param string $postId
     *
     * @return string $imageId
     */
    public function getImageId($postId)
    {
        $sql = "SELECT 
            id
            FROM {$this->tableName}            
            WHERE is_delete = 0 AND post_id = ? 
            LIMIT 1";
        
        $query = $this->connect->prepare($sql);
        $query->execute([$postId]);
        $imageId = $query->fetchColumn();
        
        return $imageId;
    }
}
