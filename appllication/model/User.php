<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Model;

use PDO as PDO;
use Framework\Model;

/**
 * Category class
 */
class User extends Model
{
    protected $tableName = 'users';

    protected static $fieldsName = [
        'first_name' => '', 
        'last_name'  => '', 
        'password'   => '',
        'birthday'   => '', 
        'country'    => '',
        'role_id'    => '', 
        'login'      => '', 
        'email'      => '',
        'phone'      => '',
        'about'      => ''
    ];

    /**
     * basicValidateRules
     * 
     * @param array $data
     *
     * @return array $rules
     */
    public static function basicValidateRules($data)
   {    
        $confirmPassword = isset($data['confirm_password']) ? $data['confirm_password'] : '';

        $rules = [            
            'first_name'   => ['isntEmpty'],
            'last_name'    => ['isntEmpty'],
            'password'     => ['isLenghtCorrect'          => [6, 16], 
                               'isEqualeToValue'          => [$confirmPassword]],
            'login'        => ['isntEmpty', 
                               'isntExistInTable'],
            'email'        => ['isitEmail',
                               'isntExistInTable'],
            'phone'        => ['isntExistInTableExceptMe']
        ];
        return $rules;
    }

    /**
     * loginValidateRules
     * 
     * @param array $data
     *
     * @return array $rules
     */
    public static function loginValidateRules($data)
   {    
        $rules = [
            'login'        => ['isExistInTable'],
            'password'     => ['isEqualeToValueFromTable' => ['login', $data['login']]]
        ];
        return $rules;
    }

    /**
     * forgotPasswordValidateRules
     * 
     * @param array $data
     *
     * @return array $rules
     */
    public static function forgotPasswordValidateRules($data)
   {    
        $rules = [
            'email'        => ['isExistInTable']
        ];
        return $rules;
    }

    /**
     * updateSettingsValidateRules
     * 
     * @param array $data
     *
     * @return array $rules
     */
    public static function updateSettingsValidateRules($data)
   {    
        $confirmPassword = isset($data['confirm_password']) ? $data['confirm_password'] : '';

        $rules = [
            'login'        => ['isntEmpty',
                               'isntExistInTableExceptMe'],
            'email'        => ['isitEmail',
                               'isntExistInTableExceptMe'],
            'password'     => ['isEqualeToValueFromTable' => ['login', $_SESSION['user']['login']]],
            'new_password' => ['isLenghtCorrect'          => [6, 16], 
                               'isEqualeToValue'          => [$confirmPassword]]
        ];
        return $rules;
    }

    /**
     * getAllUsers
     *
     * @return array $users
     */
    public function getAllUsers()
    {
        $roleModel  = new Role;

        $sql = "SELECT 
            u.*,
            r.name as role_name
            FROM {$this->tableName} as u
            LEFT JOIN {$roleModel->getTableName()} AS r ON u.role_id = r.id 
            WHERE u.is_delete = '0'";

        $query = $this->connect->prepare($sql);
        $query->execute();

        $users = $query->fetchAll(PDO::FETCH_ASSOC);
    
        return $users;
    }

    /**
     * getAllBy
     * 
     * @param string $byField
     * @param string $value
     *
     * @return array $user
     */
    public function getAllBy($byField, $value)
    {
        $roleModel  = new Role;
        $messageModel  = new Message;

        $sql = "SELECT 
            u.*,
            r.name AS role, 
            (SELECT COUNT(id) 
            FROM {$messageModel->getTableName()}
            WHERE user_to = u.id AND is_read = '0') AS messages
            FROM {$this->tableName} AS u 
            INNER JOIN {$roleModel->getTableName()} AS r ON u.role_id = r.id
            WHERE u.{$byField} = ? LIMIT 1";
        
        $query = $this->connect->prepare($sql);
        $query->execute([$value]);
        $user = $query->fetch(PDO::FETCH_ASSOC);
        
        return $user;

    }

    /**
     * getFieldBy
     * 
     * @param string $getField
     * @param string $value
     *
     * @return array $result
     */
    public function getFieldBy($getField, $value, $whereField = null)
    {
        $whereField = $whereField ?? $getField;
        
        $sql = "SELECT {$getField} FROM {$this->tableName} WHERE {$whereField} = ?";

        $query = $this->connect->prepare($sql);
        $query->execute([$value]);
        $result = $query->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * getProfileBy
     * 
     * @param string $id
     *
     * @return array $user
     */
    public function getProfileBy($id)
    {
        $imageTable = new ImageUser;
        $roleModel  = new Role;
        $postModel  = new Post;

        $sql = "SELECT 
            u.*,
            r.name as role_name,
            i.url,
            (SELECT COUNT(id) 
            FROM {$postModel->getTableName()}
            WHERE user_id = u.id AND is_delete = '0') AS posts
            FROM {$this->tableName} AS u
            LEFT JOIN {$imageTable->gettableName()} AS i ON u.id = i.user_id AND i.is_delete = '0' 
            LEFT JOIN {$roleModel->gettableName()} AS r ON u.role_id = r.id  
            WHERE u.id = ? LIMIT 1";
        
        $query = $this->connect->prepare($sql);
        $query->execute([$id]);
        $user = $query->fetch(PDO::FETCH_ASSOC);
    
        return $user;
    }
    
    /**
     * getFieldExceptMeBy
     * 
     * @param string $getField
     * @param string $value
     * @param string $whereField
     *
     * @return array $result
     */
    public function getFieldExceptMeBy($getField, $value, $whereField = null)
    {
        $whereField = $whereField ?? $getField;
        
        $sql = "SELECT {$getField} FROM {$this->tableName} WHERE {$whereField} = ? AND id NOT LIKE ? ";

        $query = $this->connect->prepare($sql);
        $query->execute([$value,
                         $_SESSION['user']['id']]);
        $result = $query->fetch(PDO::FETCH_ASSOC);

        return $result;
    }
}