<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Model;

use PDO as PDO;
use Framework\Model;

/**
 * Category class
 */
class Category extends Model 
{
    protected $tableName = 'categories';

    protected static $fieldsName = [ 
        'name'=> ''
    ];

    /**
     * basicValidateRules
     * 
     * @param array $data
     *
     * @return array $rules
     */
    public static function basicValidateRules($data)
    {
        $rules = [           
            'name' => ['isntEmpty',
                       'isntExistInTable']
        ];
        return $rules;
    }

    /**
     * getAll - select all from table
     *
     * @return array $categories
     */
    public function getAll()
    {
        $postCategiryModel = new PostCategory;
        $postModel = new Post;

        $sql = "SELECT 
            c.*,
            (SELECT COUNT(p.id) 
            FROM {$postModel->getTableName()} AS p
            RIGHT JOIN {$postCategiryModel->getTableName()} AS t ON t.post_id = p.id AND p.is_delete = 0
            WHERE c.id = t.category_id) AS posts 
            FROM {$this->tableName} AS c";
        
        $query = $this->connect->prepare($sql);
        $query->execute();
        
        $categories = $query->fetchAll(PDO::FETCH_ASSOC);

        return $categories;
    }

    /**
     * getNameBy
     * 
     * @param string $id
     *
     * @return array $category
     */
    public function getAllBy($id)
    {
        $sql = "SELECT * FROM {$this->tableName} WHERE id = ?";
        
        $query = $this->connect->prepare($sql);
        $query->execute([$id]);
        
        $category = $query->fetch(PDO::FETCH_KEY_PAIR);

        return $category;
    }

    /**
     * getFieldBy
     * 
     * @param string $nameField
     * @param string $value
     * @param string $whereField
     *
     * @return array $result
     */
     public function getFieldBy($nameField, $value, $whereField = null)
    {
        $whereField = $whereField ?? $nameField;
        
        $sql = "SELECT {$nameField} FROM {$this->tableName} WHERE {$whereField} = ?";

        $query = $this->connect->prepare($sql);
        $query->execute([$value]);
        $result = $query->fetch(PDO::FETCH_ASSOC);

        return $result;
    }
}