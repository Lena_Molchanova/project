<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Model;

use PDO as PDO;
use Framework\Model;

/**
 * PassRecovery class
 */
class PassRecovery extends Model {

    protected $tableName = 'pass_recovery';

    /**
     * save
     * 
     * @param array $user
     */
    public function save($user)
    {
        $sql = "INSERT INTO {$this->tableName} (
            user_id,
            expires,
            access_hash) 
            VALUES (?,?,?)";

        $query = $this->connect->prepare($sql);        
        $query->execute([$user['id'], 
                         $user['expires'],
                         $user['accessHash']]);
    }

    /**
     * getAllBy
     * 
     * @param string $byField
     * @param string $value
     *
     * @return array $result
     */
    public function getAllBy($byField, $value)
    {
        $sql = "SELECT * FROM {$this->tableName} WHERE {$byField} = ?";
        
        $query = $this->connect->prepare($sql);
        $query->execute([$value]);
        $result = $query->fetch(PDO::FETCH_ASSOC);
    
        return $result;
    }
}