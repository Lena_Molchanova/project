<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Model;

use PDO as PDO;
use Framework\Model;

/**
 * Post class
 */
class Post extends Model {

    protected $tableName = 'posts';

    protected static $fieldsName = [         
        'description' => '',
        'user_id'     => '',
        'content'     => '',
        'title'       => '',
        'views'       => ''
    ];

    /**
     * basicValidateRules
     * 
     * @param array $data
     *
     * @return array $rules
     */
    public static function basicValidateRules($data)
    {
        $rules = [
            'title'       => ['isntEmpty'],
            'description' => ['isntEmpty'],
            'content'     => ['isntEmpty']
        ];
        return $rules;
    }

    /**
     * getAll
     *
     * @return array $posts
     */
    public function getAll()
    {
        $imageModel = new ImagePost;
        $userModel  = new User;

        $sql = "SELECT 
            p.id, 
            p.title, 
            p.description, 
            p.user_id, 
            p.created_at, 
            p.updated_at, 
            p.views,
            u.first_name,
            u.last_name,
            i.url
            FROM {$this->tableName} AS p 
            INNER JOIN {$userModel->getTableName()} AS u ON p.user_id = u.id 
            LEFT JOIN {$imageModel->getTableName()} AS i ON p.id = i.post_id AND i.is_delete = '0'
            WHERE p.is_delete = 0
            ORDER BY p.created_at DESC LIMIT 20";
        
        $query = $this->connect->prepare($sql);
        $query->execute();
        
        $posts = $query->fetchAll(PDO::FETCH_ASSOC);

        return $posts;
    }

    /**
     * getAllbyUser
     * 
     * @param string $id
     *
     * @return array $posts
     */
    public function getAllbyUser($id)
    {
        $imageModel = new ImagePost;
        $userModel  = new User;

        $sql = "SELECT 
            p.id, 
            p.title, 
            p.description, 
            p.user_id, 
            p.created_at, 
            p.updated_at, 
            p.views,
            u.first_name,
            u.last_name,
            i.url
            FROM {$this->tableName} AS p 
            INNER JOIN {$userModel->getTableName()} AS u ON p.user_id = u.id 
            LEFT JOIN {$imageModel->gettableName()} AS i ON p.id = i.post_id  AND i.is_delete = 0 
            WHERE p.is_delete = 0 AND p.user_id = ? 
            ORDER BY p.created_at DESC LIMIT 10";
        
        $query = $this->connect->prepare($sql);
        $query->execute([$id]);
        
        $posts = $query->fetchAll(PDO::FETCH_ASSOC);

        return $posts;
    }

    /**
     * getById
     * 
     * @param string $id
     *
     * @return array $post
     */
    public function getById($id)
    {
        $imageModel = new ImagePost;
        $userModel  = new User;
        
        $sql = "SELECT 
            p.*,
            u.first_name,
            u.last_name,
            i.url
            FROM {$this->tableName} AS p 
            INNER JOIN {$userModel->getTableName()} AS u ON p.user_id = u.id 
            LEFT JOIN {$imageModel->gettableName()} AS i ON p.id = i.post_id AND i.is_delete = 0           
            WHERE p.is_delete = 0 AND p.id = ?
            LIMIT 1";
        
        $query = $this->connect->prepare($sql);
        $query->execute([$id]);
        $post = $query->fetch(PDO::FETCH_ASSOC);
    
        return $post;
    }

    /**
     * getForUpdate
     * 
     * @param string $id
     *
     * @return array $post
     */
    public function getForUpdate($id)
    {
        $imageModel = new ImagePost;

        $sql = "SELECT 
            p.id, 
            p.title,
            p.user_id,
            p.description,
            p.content,
            i.url
            FROM {$this->tableName} AS p  
            LEFT JOIN {$imageModel->gettableName()} AS i ON p.id = i.post_id AND i.is_delete = 0           
            WHERE p.id = ?
            LIMIT 1";
        
        $query = $this->connect->prepare($sql);
        $query->execute([$id]);
        $post = $query->fetch(PDO::FETCH_ASSOC);

        return $post;
    }

    /**
     * getForUpdate
     * 
     * @param string $id
     */
    public function plusView($id)
    {
        $sql = "UPDATE {$this->tableName} SET
            views = views + ?
            WHERE id = ?";

        $query = $this->connect->prepare($sql);
        $query->execute([1, $id]);
    }
}


