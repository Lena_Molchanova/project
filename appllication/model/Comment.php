<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Model;

use PDO as PDO;
use Framework\Model;

/**
 * Comment class
 */
class Comment extends Model
{
    protected $tableName = 'comments';

    protected static $fieldsName = [         
        'content'  => '',
        'user_id'  => '',
        'post_id'  => '',
        'title'    => ''
    ];

    /**
     * basicValidateRules
     * 
     * @param array $data
     *
     * @return array $rules
     */
    public static function basicValidateRules($data)
    {
        $rules = [
            'content' => ['isntEmpty']
        ];
        return $rules;
    }

    /**
     * getAllByPost
     *
     * @param string $postId
     *
     * @return array $comments
     */
    public function getAllByPost($postId)
    {
        $userModel  = new User;
        $imageModel = new ImageUser;
        
        $sql = "SELECT 
            c.id, 
            c.title, 
            c.content, 
            c.user_id, 
            c.created_at,
            u.first_name,
            u.last_name,
            i.url
            FROM {$this->tableName} AS c 
            INNER JOIN {$userModel->getTableName()} AS u ON c.user_id = u.id 
            LEFT JOIN {$imageModel->gettableName()} AS i ON c.user_id = i.user_id AND i.is_delete = '0'           
            WHERE c.is_delete = '0' AND post_id = ?
            ORDER BY c.created_at DESC";

        $query = $this->connect->prepare($sql);
        $query->execute([$postId]);

        $comments = $query->fetchAll(PDO::FETCH_ASSOC);
        
        return $comments;
    }

    /**
     * getById 
     *
     * @param string $id
     *
     * @return array $comment
     */
    public function getById($id)
    {
        $sql = "SELECT
            id, 
            title, 
            content,
            user_id
            FROM {$this->tableName}
            WHERE id = ? 
            LIMIT 1";
        
        $query = $this->connect->prepare($sql);
        $query->execute([$id]);
        $comment = $query->fetch(PDO::FETCH_ASSOC);
    
        return $comment;
    }

    /**
     * getForCount 
     *
     * @param string $postId
     *
     * @return array $comments
     */
    public function getForCount($postId)
    {
        $sql = "SELECT
            COUNT(id)
            FROM {$this->tableName}
            WHERE is_delete = '0' AND post_id = ?";
        
        $query = $this->connect->prepare($sql);
        $query->execute([$postId]);
        $comments = $query->fetchColumn();
    
        return $comments;
    }
}
