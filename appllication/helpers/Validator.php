<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Helpers;

/**
 * Validator class
 */
class Validator {

    static $errors = [];

    static $model;
    
    /**
     * check - starts validation of values by rules
     * 1. Get an array containing all keys(fields name) of $checkingVal which are exist in $rules;
     *    Did this for flexible use of the rules 
     * 2. Start rule method:
     *    -> if rule is assoc array -> start rule method $key($checkingVal, array $values);
     *    -> else -> start rule method value($checkingVal)
     *
     * @param array $data - input data;
     * @param string $model - name of using model, like 'User';
     * @param string $ruleList - name of list of rules;
     *
     * @return array $errors;
     */
    public static function check(array $data, $model, $ruleList = 'basic')
    {
        $model = "\\Appllication\\Model\\" . $model;
        static::$model = new $model;

        $validateRules = $ruleList . 'ValidateRules';
        $rules = static::$model::$validateRules($data);

        $validateValue = array_intersect_key($data, $rules);

        foreach ($validateValue as $key => $value) {                

            foreach ($rules[$key] as $keyRule => $rule) {

                is_array($rule) ? 
                static::$keyRule($rule, $data[$key], $key) : 
                static::$rule($data[$key], $key);   

            }                       
        }
        return static::$errors;
    }

    /**
     * isntEmpty - checks is value empty
     *
     * @param string $checkingVal
     * @param string $checkingField
     */
    public static function isntEmpty($checkingVal, $checkingField) 
    {
        if (empty($checkingVal)) {
            static::$errors[] = $checkingField . " can't be empty";
        }
    }

    /**
     * isntExistInTable - checks if there is a variable in the DB (must not be)
     *
     * @param array $args
     * @param string $checkingVal
     * @param string $checkingField
     */
    public static function isntExistInTable($checkingVal, $checkingField)
    {   
        $result = static::$model->getFieldBy($checkingField, $checkingVal);

        if ($result) {
            static::$errors[] = "Such " . $checkingField . " already exists";
        }
    }

    /**
     * isntExistInTableExceptMe - checks is value in DB except checkingField
     *
     * @param array $args
     * @param string $checkingVal
     * @param string $checkingField
     */
    public static function isntExistInTableExceptMe($checkingVal, $checkingField)
    {
        $result = static::$model->getFieldExceptMeBy($checkingField, $checkingVal);

        if ($result) {
            static::$errors[] = "Such " . $checkingField . " already exists";
        }
    }

    /**
     * isitEmail - checks is value an email
     *
     * @param string $checkingVal
     * @param string $checkingField
     */
    public static function isitEmail($checkingVal, $checkingField)
    {
        if (!filter_var($checkingVal, FILTER_VALIDATE_EMAIL)) {

            static::$errors[] = "This $checkingVal email address is considered valid.";

        }
    }

    /**
     * isLenghtCorrect - checks is lenght value correct
     *
     * @param array $args
     * @param string $checkingVal
     * @param string $checkingField
     */
    public static function isLenghtCorrect(array $args, $checkingVal, $checkingField)
    {
        if (strlen($checkingVal) < $args[0] || 
            strlen($checkingVal) > $args[1] ) {

                static::$errors[] = $checkingField . " must contain from " . $args[0] . " to " . $args[1] . " characters";
           }
    }

    /**
     * isExistInTable - checks is value in DB (must be)
     *
     * @param array $args
     * @param string $checkingVal
     * @param string $checkingField
     */
    public static function isExistInTable($checkingVal, $checkingField)
    {
        $result = static::$model->getAllBy($checkingField, $checkingVal);

        if (!$result) {
            static::$errors[] = "Such " . $checkingField . " doesn't exists";
        }
    }

    /**
     * isEqualeToValue - checks is value equale to confirm value
     *
     * @param string $valueThatMustEqual
     * @param string $checkingVal
     * @param string $checkingField
     */
    public static function isEqualeToValue($valueThatMustEqual, $checkingVal, $checkingField)
    {
        if ($valueThatMustEqual[0] != $checkingVal) {

            static::$errors[] = $checkingField . ' does not equals with confirm field';
        }
    }

    /**
     * isEqualeToValueFromTable - checks is value equale to the value from DB
     *
     * @param array $args
     * @param string $checkingVal
     * @param string $checkingField
     */
    public static function isEqualeToValueFromTable($args, $checkingVal, $checkingField)
    {       
        $nameFieldTable = $args[0];
        $valueFieldTable = $args[1];

        $result = static::$model->getFieldBy($checkingField, $valueFieldTable, $nameFieldTable);
        
        if ($result['password'] != $checkingVal) {
                static::$errors[] = 'Wrong ' . $checkingField;
        }
    }
}