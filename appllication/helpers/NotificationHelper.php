<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Helpers;

/**
 * NotificationHelper class
 */
class NotificationHelper 
{
    static $infoText = [
        '0' => ['The email was sent successfully',
                'Please check the mail'],
        '1' => ['This link has expired'],
        '2' => ['Password has been successfully changed'],
        '3' => ['Profile has been successfully updated']
    ];
}
