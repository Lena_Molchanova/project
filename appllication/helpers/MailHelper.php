<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Helpers;

use Appllication\Model\PassRecovery;
use DateTime as DateTime;
use DateInterval as DateInterval;

/**
 * MailHelper class
 */
class MailHelper 
{
    /**
     * resetPasswordRequest - when user fogot password
     * 1. Create accessHash, expires date, and save all into DB;
     * 2. Prepare the mail;
     * 3. Send mail;
     *
     * @param array $user - user data from DB;
     */
    public static function resetPasswordRequest(array $user)
    {
        $user['accessHash'] = md5($user['login'].$user['password'].'[freyfvfnfnf1009]'.gettimeofday(true));

        $date = new DateTime('now');
        $date->add(new DateInterval('PT1H'));
        $user['expires'] = $date->format('Y-m-d H:i:s');

        $model = new PassRecovery;
        $model->save($user);

        $subject = 'BLOG: Reset password ';

        $message = '
        <html>
        <head>
          <title>Reset password</title>
        </head>
        <body>
          <p>Hello!</p?
          <p>You told us you forgot your password. If you really did, click here to choose a new one:</p> 
          <p><a target="_blank" href="project/auth/reset/' . $user['accessHash'] . '">Reset password</a></p>
          <p>If you didnt mean to reset your password, then you can just ignore this email; your password will not change.</p>
        </body>
        </html>
        ';

        self::sendEmail($user['email'], $subject, $message);
    }

    /**
     * sendEmail - send mail
     *
     * @param string $to - email;
     * @param string $subject - mail subject;
     * @param string $message - mail text;
     */
    public static function sendEmail($to, $subject, $message)
    {
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        mail($to, $subject, $message, $headers);
    }
}