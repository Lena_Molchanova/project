<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Helpers;

/**
 * ImageHelper class
 */
class ImageHelper {

    /**
     * uploadImage - is universal for upload (At that moment - PostImage and UserImage)
     * 1. Typical download of image to a folder.
     * 2. Check is it $foreignId in 'table' DB 
     *    + -> SET is_delete = 1;
     * 3. INSERT in 'table' new line;
     *
     * @param array $newImage - upload image;
     * @param string $foreignId - foreign key, like userId or postId
     * @param string $type - type of data, like 'User' or 'Post'
     */
    public static function uploadImage($newImage, $foreignId, $type)
    {
        if($newImage['name']){

            $tmpName = $newImage['tmp_name'];
            $fileName = $newImage['name'];
            $imagePath = IMG_PATH . $type . "/". $fileName;
            $fullImagePath = ROOT . $imagePath;     
            
            move_uploaded_file($tmpName, $fullImagePath);

            $nameModel = "\\Appllication\\Model\\Image" . $type;
            $model = new $nameModel();

            if ($imageId = $model->getImageId($foreignId)) {
                $model->deleteById($imageId);
            } 
                        
            $method = $type.'ImgSaveDataPrepare';
            $model->save(PrepareSqlHelper::$method($imagePath, $foreignId));        
        }
    }
}