<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Helpers;

/**
 * PrepareSqlHelper class
 */
class PrepareSqlHelper 
{
    /** 
     * updatePrepare - Prepares the query in the database (sql) 
     * 1. Check and process $inputData;
     * 2. Keys(fields name) of the $inputData turn into a string using a 
     *    specific glue, and then replace this glue with "='" . $value . "'" 
     *
     * @param array $inputData - array from view form,
     * @param string $tableName - used table name from DB,
     * @param array $fieldsName - fields name in table (sets in models)
     *
     * @return srting $sql
     */
    public static function updatePrepare($inputData, $tableName, $fieldsName)
    {
        $data = self::prepareData($inputData, $fieldsName);
        
        $data['updated_at'] = date('Y-m-d H:i:s');

        $pieceSql = implode(' =?, ', array_keys($data)) . " =? WHERE id = ?";

        foreach ($data as $value) {
            $pieceSql = substr_replace(
                $pieceSql,
                " = '" . $value . "'", 
                strpos($pieceSql, ' =?'), 3
                );
        }

        $sql = "UPDATE {$tableName} SET " . $pieceSql;
        
        return $sql;
    }

    /** 
     * savePrepare - Prepares the query in the database (sql)
     * 1. Check and process $inputData;
     * 2. Format string 
     * 
     * @param array $inputData - array from view form,
     * @param string $tableName - used table name from DB,
     * @param array $fieldsName - fields name in table (sets in models)
     *
     * @return srting $sql
     */
    public static function savePrepare($inputData, $tableName, $fieldsName)
    {
        $data = self::prepareData($inputData, $fieldsName);

        $sql = sprintf(
            'INSERT INTO %s (%s) VALUES (%s)',
            $tableName,
            implode(', ', array_keys($data)),
            "'" . implode("', '", $data) . "'"
        );

        return $sql; 
    }

    /**
     * prepareData - Check and process $inputData;
     * 1. Get an array containing all keys(fields name) of $inputData which are exist in $fieldsName;
     *    Did it, because in $inputData there may be extra fields, like 'confirm_password'
     * 2. Quote values from $inputData with slashes;
     * 3. Removes empty value
     *
     * @param array $inputData - array from view form,
     * @param array $fieldsName - fields name in table (sets in models)
     *
     * @return array $data
     */
    public static function prepareData($inputData, $fieldsName)
    {   
        $data = array_intersect_key($inputData, $fieldsName);
        $data = array_map("addslashes", $data);

        foreach ($data as $key => $value) {
            if ($value == '') {
                unset($data[$key]);
            }
        }
        return $data;
    }

    /**
     * postSaveDataPrepare - prepare data for save
     *
     * @param array $data - array of data from view form
     *
     * @return array $data
     */
    public static function postSaveDataPrepare($data)
    {
        $data['user_id'] = $_SESSION['user']['id'];

        return $data;
    }

    /**
     * commentSaveDataPrepare - prepare data for save
     *
     * @param array $data - array of data from view form
     * @param string $postId - post id
     *
     * @return array $data
     */
    public static function commentSaveDataPrepare($data, $postId)
    {
        $data['user_id'] = $_SESSION['user']['id'];
        $data['post_id'] = $postId;
        
        return $data;
    }

    /**
     * userImgSaveDataPrepare - prepare data for save
     *
     * @param string $path - image path
     * @param string $userId - user id
     *
     * @return array $data
     */
    public static function userImgSaveDataPrepare($path, $userId)
    {
        $data['url']     = $path;
        $data['user_id'] = $userId;
        
        return $data;
    }

    /**
     * postImgSaveDataPrepare - prepare data for save
     *
     * @param string $path - image path
     * @param string $postId - post id
     *
     * @return array $data
     */
    public static function postImgSaveDataPrepare($path, $postId)
    {
        $data['url']     = $path;
        $data['post_id'] = $postId;

        return $data;
    }

    /**
     * messageSaveDataPrepare - prepare data for save
     *
     * @param array $data - array of data from view form
     * @param string $toUserId - to user id
     *
     * @return array $data
     */
    public static function messageSaveDataPrepare($data, $toUserId)
    {
        $data['user_to']   = $toUserId;
        $data['user_from'] = $_SESSION['user']['id'];
        
        return $data;
    }

    /**
     * categorySaveDataPrepare - prepare data for save
     *
     * @param array $data - array of data from view form
     *
     * @return array $data
     */
    public static function categorySaveDataPrepare($data)
    {
        $data['name'] = ucfirst(strtolower(trim($data['name'])));
        
        return $data;
    }
}