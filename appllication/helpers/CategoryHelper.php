<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Helpers;

use Appllication\Model\Post;
use Appllication\Model\Category;
use Appllication\Model\PostCategory;

/**
 * CategoryHelper class
 *
 * @todo refactoring
 */
class CategoryHelper {

    /**
     * savePostCategories - INSERT it table DB (many to many) foreign keys post and cstegories;
     *
     * @param string $postId - post id
     * @param array $categories -category ids (input data from checkbox)
     */ 
    public static function savePostCategories($postId, $categories)
    {
        $postCategoryModel = new PostCategory();
        extract($categories);
        foreach ($categories as $category) {
            $postCategoryModel->save(['post_id'=>$postId,
                                      'category_id'=>$category]);
        }
    }

    /**
     * updateCategoriesByPost - to update categories in a post
     *
     * @param $postId - post id
     * @param $categories - category ids (inpet data from checkbox)
     * @param $postCategories - categories that are in post now
     */
    public static function updateCategoriesByPost($postId, $categories, $postCategories)
    {
        $postCategoryModel = new PostCategory();

        for ($i=0; $i < count($postCategories); $i++) { 

            foreach ($postCategories[$i] as $id => $postCategory) {
                
                if ($key = array_search($id, $categories)) {
                    
                    unset($categories[$key]);

                } else {

                    $postCategoryModel->realDeleteBy($id, $postId);
                }
            }
        }

        self::savePostCategories($postId, $categories);
    }

    /**
     * getCategoriesPosts - SELECT for each post of its categories
     * 1. for each post id SELECT category id (from many to many table);
     * 2. and now SELECT all FROM categories table BY category id;
     *
     * @param array $posts - array of posts
     *
     * @return array $postWithCategoryName where is category name now
     */ 
    public static function getCategoriesPosts($posts)
    {
        $postCategoryModel = new PostCategory();
        $categoryModel = new Category();
        $postWithCategoryName = [];

        extract($posts);

        foreach ($posts as $post) {

            $categoriesId = $postCategoryModel->getCategoriesIdByPost($post['id']);

            if ($categoriesId) {

                foreach ($categoriesId as $key => $id) {
                    
                    $post['category'][] = $categoryModel->getAllBy($id['category_id']);                 
                }               
            }
            array_push($postWithCategoryName, $post);
        }
        return $postWithCategoryName;
    }

    /**
     * getCategoryByPost - SELECT for ONE post its categories
     * 1. for post id SELECT categories id (from many to many table);
     * 2. and now SELECT all FROM categories BY category id;
     *
     * @param string $id - post id
     *
     * @return array $postCategories - categories
     */ 
    public static function getCategoryByPost($id)
    {
        $postCategoryModel = new PostCategory();
        $categoryModel = new Category();
        $postCategories = [];

        $categoriesId = $postCategoryModel->getCategoriesIdByPost($id);

        if ($categoriesId) {

            foreach ($categoriesId as $key => $id) {
                
                $postCategories[] = $categoryModel->getAllBy($id['category_id']);   
            }   
        }
        return $postCategories;
    }

    /**
     * method getAllPostsByCategory - SELECT for category id all posts
     * 1. for category id SELECT posts id (from many to many table);
     * 2. and now SELECT all FROM posts BY post id;
     *
     * @param string $id - category id
     *
     * @return array $posts
     */
    public static function getAllPostsByCategory($id)
    {
        $postCategoryModel = new PostCategory();
        $postModel = new Post();
        $posts = [];

        $postsId = $postCategoryModel->getPostsIdByCategory($id);    

        foreach ($postsId as $key => $id) {

            if ($postModel->getById($id['post_id'])) {
                $posts[] = $postModel->getById($id['post_id']);
            }
        }
        krsort($posts);
        return $posts;
    }
}