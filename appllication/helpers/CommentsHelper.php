<?php
/**
 * @author Lena Molchanova
 */

namespace Appllication\Helpers;

use Appllication\Model\Comment;

/**
 * CommentHelper class
 */
class CommentHelper 
{
    /**
     * countCommentsPost - count comments by posts;
     *
     * @param array $posts - array of posts
     *
     * @return array $posts
     */
    public static function countCommentsPost($posts)
    {
        $model = new Comment();

        extract($posts);
        
        for ($i=0; $i < count($posts); $i++) {
        
            $posts[$i]['comments'] = $model->getForCount($posts[$i]['id']);
        }

        return $posts;
    }
}