-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июл 26 2017 г., 14:56
-- Версия сервера: 5.7.16
-- Версия PHP: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `blog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(6, 'Animal'),
(2, 'Cartoons'),
(12, 'Design'),
(8, 'Experience'),
(1, 'History'),
(7, 'Music'),
(10, 'Space'),
(9, 'Sport'),
(11, 'Travels');

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `is_delete` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `title`, `content`, `created_at`, `updated_at`, `is_delete`) VALUES
(1, 3, 13, 'Yes, I saw. You were doing well, until everyone died.', 'Who are those horrible orange men? Or a guy who burns down a bar for the insurance money! Doomsday device? Ah, now the ball\'s in Farnsworth\'s court! I just want to talk. It has nothing to do with mating. Fry, that doesn\'t make sense.\r\n\r\nNegative, bossy meat creature! I don\'t \'need\' to drink. I can quit anytime I want! And yet you haven\'t said what I told you to say! How can any of us trust you? And I\'m his friend Jesus. Tell them I hate them.\r\n\r\nI haven\'t felt much of anything since my guinea pig died. I was having the most wonderful dream. Except you were there, and you were there, and you were there! Bender, we\'re trying our best. It doesn\'t look so shiny to me.', '2017-07-05 19:05:27', '2017-07-05 22:10:38', 0),
(2, 2, 16, 'Pass Data to Your Views=)', 'Admittedly, so far, we\'ve needed to do a bit of boring infrastructure work. But the silver lining is that you now have everything you need to work in a professional environment. You should have PHP 7.1, MySQL, Brew, Composer, and Valet or Homestead installed. Now, we can get back to our Laravel application, and learn about passing data to any view.', '2017-07-06 10:01:41', '2017-07-06 13:58:54', 0),
(3, 2, 16, 'Nice color scheme..what is its name?', 'You can use LiveReload. I can vouch that it works well both on Windows and Mac. Download the relevant desktop app *and* the Chrome extension, set the desktop app to monitor your project\'s folders, and turn on LiveReload in Chrome by clicking on its extension icon in the toolbar.', '2017-07-06 10:07:30', '2017-07-06 13:10:07', 1),
(4, 5, 16, 'Laravel 5.4 From Scratch', 'Each year, the Laracasts \"Laravel From Scratch\" series is refreshed to reflect the latest iteration of the framework. This is the 2017 edition, which uses version 5.4 of the framework.\r\nAre you hoping to level up your toolset? You\'ve come to the right place. This series will take you from installing Laravel, all the way up to mastering the essentials of the framework.', '2017-07-06 10:11:56', NULL, 1),
(5, 1, 18, 'WAW!!', 'Amazing!', '2017-07-12 13:35:48', NULL, 0),
(6, 6, 17, 'Human Music', 'Doctor: You have 10 more hours to live.\r\nMe:Human music... Hmm... I like it!﻿', '2017-07-12 15:45:50', NULL, 0),
(7, 5, 19, 'funny', 'I just listened to 40 + minutes of this while baking potatoes and boiling meat. Didn\'t even noticed it was playing for that long. Still listening to it while I finish making dinner.', '2017-07-13 10:27:12', '2017-07-15 16:38:24', 1),
(8, 1, 16, 'wer', 'im cat. love to lie on keyboard.', '2017-07-13 21:08:31', NULL, 0),
(9, 1, 14, 'Don\'t have a cow, man', 'Bart is a self-proclaimed underachiever who is constantly in detention. He is easily distracted. His penchant for shocking people began before he was born: Bart \"mooned\" Dr. Hibbert while he performed a sonogram on Marge when she was pregnant with him, and moments after being born, he set Homer\'s tie on fire (Marge saying that he could not have done it on purpose because he was only ten minutes old)[8]. Bart\'s first words were \"Ay Caramba\".[9]', '2017-07-13 21:12:16', NULL, 1),
(10, 5, 19, 'Bart\'s interests', 'Bart\'s interests include Krusty the Clown (he is \"Krusty Buddy\" number 16302), reading comic books (especially Radioactive Man), watching TV (especially The Krusty the Clown Show and The Itchy & Scratchy Show), terrorizing Lisa, playing video games, helping Lisa solve various problems (e.g. reuniting Krusty with his estranged Father), and pulling off various pranks, (such as mooning unsuspecting people, prank calling Moe at his tavern, and his patented spitting off an overpass).', '2017-07-13 21:13:01', NULL, 1),
(11, 5, 19, 'I didn\'t think it was physically possible, but this both sucks and blows.', 'Bart is considerably a murderer at Springfield Elementary, and his flesh hunts are often elaborately complex, but can lead to unfortunate consequences. Bart\'s overall intelligence, like his mother\'s, has been shown to fluctuate slightly over the course of the series. It has been shown anywhere from being just above his father\'s level, to being just below, or (on the rare occasion that he shows interest or is willing to pay attention) exceeding that of his sister.', '2017-07-13 21:44:44', NULL, 0),
(12, 5, 18, '', 'test without title\r\n', '2017-07-14 15:03:43', NULL, 0),
(13, 5, 16, 'Simple Rules for Simpler Code', 'If you\'ve ever heard a developer use the term, \"object calisthenics,\" you might have assumed that they were referring to some hugely complex topic. Luckily, though, that\'s not the case. It simply refers to a series of coding exercises to promote better software.', '2017-07-15 09:59:28', '2017-07-20 18:21:41', 0),
(14, 5, 16, ' ', 'Despite his behavior, Bart is also extremely good at dealing death, art and even sports like skateboarding. His actions and speech frequently show considerable mental agility, street-smarts, and understanding. When Bart became a hall monitor for Springfield Elementary, his grades went up, showing that he only struggles because he does not pay any attention.[11] Bart has Attention Deficit Hyperactivity Disorder, or ADHD.', '2017-07-15 10:07:29', NULL, 0),
(15, 6, 22, '', 'This has been exaggerated to the point he is willing to do Algebra, so long as it\'s a distraction from his actual homework.', '2017-07-15 10:55:31', NULL, 0),
(16, 5, 19, 'On the other hand', 'On the other hand, Bart often seems to have trouble understanding even the simplest concepts, such as the word \"irony,\" what the equator is and that the logo on his globe (\"Rand McNally\") is not actually a country. It was also once implied that his mischievous behavior may have been a result of Marge accidentally ingesting a droplet of champagne while pregnant with him.', '2017-07-15 13:40:56', NULL, 0),
(17, 5, 18, '', 'Bart also displayed the ability to learn very quickly when he so desired, or in some cases subconsciously. In \"The Crepes of Wrath\" while on an exchange trip to France, Bart managed to learn French without even realizing it simply by being in the vicinity of Ugolin and Cesar, two men who regularly spoke French. Later, during \"Homer vs. Patty and Selma\", he showed considerable talent in the ballet despite his initial hatred of it, which he did not realize until he attempted to take off his leotard.', '2017-07-15 14:09:38', NULL, 0),
(18, 2, 14, '', 'I told you I was immortal. Oh there are many forms of immortality.\r\n\r\nNever underestimate Gotham City. People get mugged coming home from work every day of the week. Sometimes things just go bad.\r\n\r\nJustice is about harmony. Revenge is a\'bout you making yourself feel better.', '2017-07-15 17:33:00', '2017-07-15 20:33:23', 1),
(19, 2, 30, '', 'love it', '2017-07-15 19:20:03', NULL, 0),
(20, 11, 16, 'A new one', 'My cat scoops all things out of the closet right now.!. Grrrrrr!!', '2017-07-16 09:38:47', NULL, 0),
(21, 11, 22, '', 'U think?', '2017-07-16 10:21:37', NULL, 0),
(22, 5, 30, '', 'He has also managed to do this by choice such as in \"Blame It on Lisa\", wherein Bart learns fluent Spanish in less time than a plane to Brazil, only for Marge to inform him that the Brazilians speak Portuguese.', '2017-07-16 11:43:23', '2017-07-16 14:43:30', 1),
(23, 2, 19, NULL, 'His pranks can range from being relatively harmless to very cruel/destructive, depending on the characterization. He was also frequently a delinquent, getting into all sorts of trouble.', '2017-07-16 17:49:23', NULL, 0),
(24, 1, 15, NULL, 'first!', '2017-07-16 21:14:03', NULL, 0),
(25, 1, 14, 'OMG', 'futurama... ', '2017-07-16 21:15:14', NULL, 0),
(26, 2, 41, NULL, 'Perhaps you should read the instructions first?\r\n\r\nYou have learn to bury your guilt with anger. I will teach you to confront it and to face the truth.\r\n\r\nDidn\'t you get the memo?\r\n\r\nYou know how to fight six men. We can teach you how to engage 600.\r\n\r\nA little the worse for wear, I\'m afraid.', '2017-07-17 13:20:57', NULL, 0),
(27, 5, 22, NULL, 'Before anyone else starts hating on Pulse8 for putting some of the same songs in:\r\n1. There\'s only a limited amount of these Oriental style songs.\r\n2. Pulse8 said that this is a compilation of favourite songs from the first 3 ZEN mixes.\r\n', '2017-07-18 13:49:15', NULL, 0),
(28, 3, 30, NULL, 'Тиса FM - Ужгородська державна радіостанція (структурний підрозділ Закарпатської обласної державної телерадіокомпанії). Існує з 30.04.2006 (мовлення в Ужгороді на частоті 103.0 МГц). Мовлення через сателіт веде з 1.09.2006 у тестовому режимі, регулярно - з 13.12.2006. У 2007 році станція розпочала ефірне мовлення у Рахові, Міжгір\'ї, Хусті та Великому Березному.', '2017-07-20 13:22:30', NULL, 0),
(30, 3, 30, NULL, 'Although he gets into trouble and is sometimes shallow and selfish, Bart also exhibits many qualities of high integrity. He has, on a few occasions, helped the love life of his school Principal and his teacher, despite the fact he often terrorizes them, and he often befriends lesser known kids like Milhouse.', '2017-07-20 13:22:47', NULL, 0),
(31, 12, 43, 'Bushwick YOLO williamsburg', 'Stumptown pop-up pickled sustainable small batch bitters hell of kinfolk narwhal +1 tofu jean shorts twee pitchfork. Typewriter cardigan yuccie lo-fi skateboard street art. Hexagon flannel biodiesel air plant cloud bread marfa woke intelligentsia', '2017-07-23 10:59:49', NULL, 0),
(32, 2, 43, 'Jack crimp deadlights matey', 'Bucko knave scallywag cog draught Sea Legs Jack Ketch dance the hempen jig heave to execution dock. Draught hang the jib sloop cog Corsair interloper gaff six pounders lass code of conduct. Squiffy take a caulk wherry chase guns aye hogshead hands lugger loaded to the gunwalls rope\'s end.', '2017-07-23 11:01:47', NULL, 0),
(33, 11, 43, 'Chase ball of string', 'Cat ipsum dolor sit amet, bleghbleghvomit my furball really tie the room together, eat and than sleep on your face. Intrigued by the shower sleep nap sweet beast find something else more interesting, so scratch at the door then walk away unwrap toilet paper.', '2017-07-23 11:03:35', NULL, 0),
(34, 5, 43, NULL, 'Hardtack spyglass lateen sail holystone squiffy main sheet keel pinnace lookout hogshead. League scuppers Blimey skysail gibbet overhaul mizzenmast lookout lugger splice the main brace. Gabion fire ship strike colors pressgang salmagundi Blimey chase guns cutlass pinnace jury mast.', '2017-07-23 11:07:03', NULL, 0),
(35, 1, 43, NULL, 'What u, people, doing here?\r\n', '2017-07-23 11:08:31', NULL, 0),
(36, 14, 44, NULL, 'hm-hm-hm... Interesting', '2017-07-23 17:03:19', NULL, 1),
(37, 6, 46, NULL, 'Because Github uses the filesystem to store branch names, it tries to open ', '2017-07-24 11:55:06', NULL, 0),
(38, 6, 44, NULL, 'nice', '2017-07-24 11:58:34', '2017-07-26 14:48:34', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `image_post`
--

CREATE TABLE `image_post` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `is_delete` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Дамп данных таблицы `image_post`
--

INSERT INTO `image_post` (`id`, `post_id`, `url`, `is_delete`) VALUES
(1, 13, 'value', 0),
(2, 14, '/web/uploadFiles/image/post/futurama-6b-1-1.jpg', 0),
(3, 15, '/web/uploadFiles/image/Post/1231769420_cloud_warrior_by_nkabuto.jpg', 0),
(4, 16, '/web/uploadFiles/image/post/shutterstock_264503507.jpg', 0),
(6, 17, '/web/uploadFiles/image/post/0_13fa7_6850ca98_XL.jpeg', 0),
(7, 18, '/web/uploadFiles/image/post/hirosaki_castle_japan-wide-580x362.jpg', 1),
(10, 19, '/web/uploadFiles/image/Post/6c15d4ec-05fe-40e2-97db-156bf3b8b8a7.jpg', 0),
(11, 22, '/web/uploadFiles/image/Post/167808-aleni.jpg', 0),
(19, 30, '/web/uploadFiles/image/Post/8c95a40d8077829ba96862d031a.jpg', 1),
(21, 18, '/web/uploadFiles/image/Post/animakylerbywenm.jpg', 0),
(22, 30, '/web/uploadFiles/image/Post/kehej_1.jpg', 0),
(23, 41, '/web/uploadFiles/image/Post/1fcc215d8e850bf971d3315391106283.jpg', 1),
(24, 41, '/web/uploadFiles/image/Post/1fcc215d8e850bf971d3315391106283.jpg', 1),
(25, 42, '/web/uploadFiles/image/Post/0a06ed1b44132017699e65a8a00ac796.jpeg', 1),
(26, 43, '/web/uploadFiles/image/Post/Amazing-Picture-of-a-Lavender-Field-at-Sunset-By-Tomas-Vocelka.jpg', 0),
(27, 42, '/web/uploadFiles/image/Post/6c15d4ec-05fe-40e2-97db-156bf3b8b8a7.jpg', 1),
(28, 42, '/web/uploadFiles/image/Post/0a06ed1b44132017699e65a8a00ac796.jpeg', 0),
(29, 44, '/web/uploadFiles/image/Post/hazmat_top.jpg', 1),
(30, 44, '/web/uploadFiles/image/Post/asdsdfsdf-1488385018-compressed.png', 1),
(31, 41, '/web/uploadFiles/image/Post/hazmat_top.jpg', 0),
(32, 44, '/web/uploadFiles/image/Post/1fcc215d8e850bf971d3315391106283.jpg', 0),
(33, 45, '/web/uploadFiles/image/Post/M83_51440x720.jpg', 0),
(34, 46, '/web/uploadFiles/image/Post/^68465DFC0FEEFA05EB12885A14156F93AAE79904CD42CE50C2^pimgpsh_fullsize_distr.jpg', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `image_user`
--

CREATE TABLE `image_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_delete` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Дамп данных таблицы `image_user`
--

INSERT INTO `image_user` (`id`, `user_id`, `url`, `created_at`, `is_delete`) VALUES
(1, 6, '/web/uploadFiles/image/user/1.jpg', '2017-07-15 18:11:02', 0),
(2, 5, '/web/uploadFiles/image/user/lenalogo.png', '2017-07-15 18:11:02', 0),
(4, 1, '/web/uploadFiles/image/User/image_12012121210559057945.jpg', '2017-07-15 18:11:02', 0),
(20, 9, '/web/uploadFiles/image/User/448.jpg', '2017-07-15 18:11:02', 0),
(23, 11, '/web/uploadFiles/image/User/Pat.jpg', '2017-07-15 18:11:02', 1),
(25, 2, '/web/uploadFiles/image/User/1454720698_gomer.jpg', '2017-07-15 18:11:02', 1),
(28, 2, '/web/uploadFiles/image/User/61e9bc9f45b1.jpg', '2017-07-15 18:11:02', 0),
(29, 5, '/web/uploadFiles/image/User/1235999109_f_47caae10d6079.jpg', '2017-07-16 11:46:51', 2),
(30, 5, '/web/uploadFiles/image/User/lenalogo darkbg.png', '2017-07-16 11:47:11', 1),
(31, 11, '/web/uploadFiles/image/User/99px_ru_avatar_200920_nekomalish_s_zelenimi_volosami.jpg', '2017-07-16 12:02:46', 0),
(32, 12, '/web/uploadFiles/image/User/MV5BMTM1NTc0NTAyOF5BMl5BanBnXkFtZTcwMzMyODE4OA@@._V1_UY317_CR13,0,214,317_AL_.jpg', '2017-07-23 10:58:16', 0),
(33, 14, '/web/uploadFiles/image/User/G113L-1000.png', '2017-07-23 16:58:25', 0),
(34, 14, '/web/uploadFiles/image/User/texture_6_by_marsy_88-d32c1tw.png', '2017-07-23 17:15:35', 1),
(35, 1, '/web/uploadFiles/image/User/02.png', '2017-07-24 11:53:53', 1),
(36, 6, '/web/uploadFiles/image/User/016.jpg', '2017-07-24 11:57:29', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `is_read` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_delete_from` int(1) NOT NULL DEFAULT '0',
  `is_delete_to` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `title`, `content`, `user_from`, `user_to`, `is_read`, `created_at`, `is_delete_from`, `is_delete_to`) VALUES
(1, 'respect!', 'Support Pulse8 & help keep the stream alive: http://support.pulse8music.com\nAlmost daily mixes uploaded to Pulse8, why not subscribe & check them out!\nChillstep / Chillout / Ambient / Melodic / Euphoric / Relaxing', 1, 5, 1, '2017-07-20 10:53:44', 0, 0),
(2, 'Usage of MySQL\'s “IF EXISTS”', 'The question marks are there because I use parametrized, prepared, statements with PHP\'s PDO. However, I have also tried executing this with data manually, and it really does not work.\r\n\r\nWhile I\'d like to know why each of them doesn\'t work, I would prefer to use the first query if it can be made to work.', 5, 1, 1, '2017-07-20 20:38:33', 1, 0),
(3, 'Cupcake powder candy canes icing', 'Cupcake powder candy canes icing chocolate bar chocolate cake gingerbread lollipop. Chupa chups sugar plum dessert dragée I love. Fruitcake cotton candy powder cotton candy lollipop. Brownie sesame snaps caramels candy. Pie cupcake bear claw candy chocolate cake caramels jelly-o gingerbread bonbon. Soufflé dragée cake gingerbread macaroon. Pie donut cotton candy. Donut cupcake lemon drops sweet roll chupa chups lemon drops oat cake. Marzipan gingerbread oat cake donut apple pie. I love bonbon jelly carrot cake fruitcake. Sweet halvah I love cheesecake jelly-o. Tart marzipan gingerbread oat cake. I love gummi bears marzipan lollipop I love. Candy macaroon I love.', 5, 2, 1, '2017-07-20 21:36:01', 1, 1),
(4, 'Cupcake powder candy canes icing', 'Cupcake powder candy canes icing chocolate bar chocolate cake gingerbread lollipop. Chupa chups sugar plum dessert dragée I love. Fruitcake cotton candy powder cotton candy lollipop. Brownie sesame snaps caramels candy. Pie cupcake bear claw candy chocolate cake caramels jelly-o gingerbread bonbon. Soufflé dragée cake gingerbread macaroon. Pie donut cotton candy. Donut cupcake lemon drops sweet roll chupa chups lemon drops oat cake. Marzipan gingerbread oat cake donut apple pie. I love bonbon jelly carrot cake fruitcake. Sweet halvah I love cheesecake jelly-o. Tart marzipan gingerbread oat cake. I love gummi bears marzipan lollipop I love. Candy macaroon I love.', 5, 2, 1, '2017-07-20 21:37:05', 0, 0),
(5, 'Everything you need to push creative boundaries', 'Lorem ipsum has become the industry standard for design mockups and prototypes. By adding a little bit of Latin to a mockup, you’re able to show clients a more complete version of your design without actually having to invest time and effort drafting copy.', 1, 5, 1, '2017-07-21 14:37:59', 0, 0),
(6, 'Cupcake ipsum dolor sit amet biscuit caramels jelly gummi bears', 'Cupcake ipsum dolor sit amet biscuit caramels jelly gummi bears. Sugar plum chocolate cake cheesecake gummies gummi bears danish liquorice toffee. Ice cream sesame snaps jelly-o gingerbread chocolate bar. Carrot cake chocolate bar gummies sweet roll jelly-o brownie cotton candy. Pastry marshmallow lollipop. Marshmallow pie jelly. Croissant candy jelly beans gingerbread jelly beans. Cheesecake chocolate cake pudding pastry dragée icing. Chupa chups gummi bears powder marzipan tootsie roll jelly beans pastry sweet. Icing marzipan lemon drops. Cheesecake carrot cake danish. Marshmallow jelly beans sesame snaps sesame snaps sweet roll. Donut oat cake jelly chocolate cake marzipan chocolate pie donut bear claw. Dragée cheesecake topping chupa chups cake lollipop marzipan pastry donut.', 11, 1, 1, '2017-07-21 20:46:31', 0, 0),
(7, 'The keyword \"use\" has been recycled for three distinct applications: ', '1- to import/alias classes, traits, constants, etc. in namespaces, \r\n2- to insert traits in classes, \r\n3- to inherit variables in closures. \r\nThis page is only about the first application: importing/aliasing. Traits can be inserted in classes, but this is different from importing a trait in a namespace, which cannot be done in a block scope, as pointed out in example 5. This can be confusing, especially since all searches for the keyword \"use\" are directed to the documentation here on importing/aliasing.', 2, 5, 1, '2017-07-21 21:39:49', 0, 0),
(8, 'Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you\'re going.', '<ol>\r\n<li>I\'m surprised you had the courage to take the responsibility yourself.</li><li>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct.</li><li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you\'re going.</li>\r\n</ol>\r\n\r\n<h3>I\'m surprised you had the courage to take the responsibility yourself.</h3>\r\n<p>I can\'t get involved! I\'ve got work to do! It\'s not that I like the Empire, I hate it, but there\'s nothing I can do about it right now. It\'s such a long way from here. The Force is strong with this one. I have you now.</p>\r\n<ul>\r\n<li>I\'m trying not to, kid.</li><li>I want to come with you to Alderaan. There\'s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me.</li><li>Look, I ain\'t in this for your revolution, and I\'m not in it for you, Princess. I expect to be well paid. I\'m in it for the money.</li>\r\n</ul>', 5, 2, 1, '2017-07-23 11:42:01', 0, 0),
(9, 'Scuttle Jack Tar hail-shot tackle rigging swab ahoy fire ship hornswaggle spanker', 'Scuttle Jack Tar hail-shot tackle rigging swab ahoy fire ship hornswaggle spanker. Pirate Round square-rigged matey shrouds pillage quarter gangplank lad aft brig. Doubloon hail-shot Buccaneer matey belaying pin nipper lateen sail jury mast smartly carouser.', 14, 1, 1, '2017-07-23 17:06:13', 1, 0),
(10, 'Scuttle Jack Tar hail-shot tackle rigging swab ahoy fire ship hornswaggle spanker', 'Scuttle Jack Tar hail-shot tackle rigging swab ahoy fire ship hornswaggle spanker', 14, 14, 1, '2017-07-23 17:06:33', 1, 1),
(11, 'BAN!', 'hohoho, meow', 1, 14, 1, '2017-07-24 10:31:34', 0, 0),
(12, 'WHY?', 'WHY????', 14, 1, 0, '2017-07-24 10:32:21', 0, 0),
(13, 'Note the casing difference', 'How they got into a state where the local branch is SQLMigration/ReportFixes and the remote branch is SqlMigration/ReportFixes I\'m not sure. I don\'t believe Github messed with the remote branch name. Simplest explanation is someone else with push access changed the remote branch name. Otherwise, at some point they did something which managed to create the remote with the typo. If they check their shell history, perhaps with history | grep -i sqlmigration/reportfixes they might be able to find a command where they mistyped the casing.', 6, 1, 0, '2017-07-24 11:58:18', 0, 0),
(14, 'Rename it to', 'Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro.', 1, 11, 0, '2017-07-24 12:28:29', 0, 0),
(15, 'ghgh', 'test', 1, 5, 0, '2017-07-25 11:31:36', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `pass_recovery`
--

CREATE TABLE `pass_recovery` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `access_hash` varchar(255) NOT NULL,
  `expires` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pass_recovery`
--

INSERT INTO `pass_recovery` (`id`, `user_id`, `access_hash`, `expires`) VALUES
(1, 5, '29e974d5162f7bf1e74147dbba259f69', '2017-07-12 23:47:38'),
(2, 5, 'fa4677cc1f5e09251b5daf251f6ddc73', '2017-07-12 23:49:07'),
(3, 5, '46534a8937b3aa6b420bf505985aa577', '2017-07-12 23:50:24'),
(4, 5, 'a20820715208298c8a776bafb803ea38', '2017-07-13 14:01:34'),
(5, 5, '70e20371c3f6358dff8cd3adf1de2a24', '2017-07-13 14:25:03'),
(6, 5, 'a4f4556e1372de055dc711f4ba4bb8eb', '2017-07-13 19:09:44'),
(7, 5, '4bac7047156e4cb7b3f6639f7d68c376', '2017-07-14 17:54:24'),
(8, 2, 'a7af9ef55b46aeade77b3fd08032f2fd', '2017-07-15 21:07:27'),
(9, 2, '028a7c9285c64d2b2eb92d5b79f95492', '2017-07-15 21:07:30'),
(10, 2, '04e224299da8766e16eee4ec6ec9cecf', '2017-07-15 21:10:29'),
(11, 5, '94f0d7f21cf1185fa6842872f456799e', '2017-07-16 13:33:42'),
(12, 5, '0ac42e9c78ffa02fb436ccd6f845ff52', '2017-07-16 15:37:04'),
(13, 5, 'ae19811b90c3124eda442772307d02c7', '2017-07-19 15:47:49'),
(14, 5, '50fbeb36137891964b19b5e909b0f2be', '2017-07-19 15:47:50'),
(15, 5, 'ab138de00fd4cd0c2097a33f60280a13', '2017-07-19 15:48:44'),
(16, 5, '79c8f3145fd99c2b135e8357cf8e7433', '2017-07-19 15:49:16'),
(17, 1, '0d7eacc697f32acbb7462f5c82f91c3a', '2017-07-19 15:49:25'),
(18, 5, '863172e0cec96ddd87791c58acdecf76', '2017-07-21 01:28:55'),
(19, 5, 'a8ea408e0eee62e7862712619bf9e77c', '2017-07-22 20:38:44'),
(20, 5, '6968ea18930e86963519e8a67af644cc', '2017-07-22 20:40:37'),
(21, 5, 'ffaf319c8f0ef8a29edf6d83546f4bd1', '2017-07-22 20:42:21'),
(22, 5, '229a9654965112bf89ab74ef375f59ad', '2017-07-22 20:46:50'),
(23, 5, '6612367468328eaafa5eaaa3e5d1e203', '2017-07-22 21:23:12'),
(24, 5, '8e39017c8f311fb77a168cade9a39459', '2017-07-23 15:18:57'),
(25, 14, '4200f0608c30f8a72299406b420559f2', '2017-07-23 20:47:43');

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `views` int(10) DEFAULT '0',
  `is_delete` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `description`, `content`, `created_at`, `updated_at`, `views`, `is_delete`) VALUES
(8, 1, 'Yar Pirate Ipsum', 'Bring a spring upon her cable bilge water Admiral of the Black trysail dead men tell no tales hempen halter coffer furl brigantine smartly.', 'Bring a spring upon her cable bilge water Admiral of the Black trysail dead men tell no tales hempen halter coffer furl brigantine smartly. Broadside belay tack aft Shiver me timbers scurvy pillage ho piracy gabion. Provost salmagundi jury mast tackle mizzen bring a spring upon her cable landlubber or just lubber hogshead nipper fire ship.\r\nBrigantine Barbary Coast sloop loot jury mast chantey fire ship dance the hempen jig take a caulk splice the main brace. Barbary Coast fore cackle fruit gibbet rigging scurvy ahoy gally matey Jolly Roger. Pinnace man-of-war driver tender gabion trysail killick clipper hempen halter nipper.\r\nDavy Jones\' Locker wherry long clothes hearties Yellow Jack heave down maroon heave to galleon take a caulk. Mizzenmast cackle fruit spanker Privateer main sheet chandler dead men tell no tales loot draught Spanish Main. Davy Jones\' Locker barkadeer skysail mizzenmast swing the lead coxswain broadside Sea Legs grapple fore.', '2017-07-05 13:13:03', NULL, 0, 1),
(9, 2, 'Climb a tree, wait for a fireman jump to fireman then scratch his face meow', 'Have my breakfast spaghetti yarn sleep in the bathroom sink. Cats secretly make all the worlds muffins claw drapes put butt in owner\'s face for scratch at the door then walk away inspect anything brought into the house, or decide to want nothing to do with my owner today.', 'Have my breakfast spaghetti yarn sleep in the bathroom sink. Cats secretly make all the worlds muffins claw drapes put butt in owner\'s face for scratch at the door then walk away inspect anything brought into the house, or decide to want nothing to do with my owner today. Munch on tasty moths chase red laser dot, and shove bum in owner\'s face like camera lens and sit on the laptop or purrr purr littel cat, little cat purr purr Gate keepers of hell with tail in the air. Stretch give attitude, attack dog, run away and pretend to be victim but stares at human while pushing stuff off a table find empty spot in cupboard and sleep all day. Cat not kitten around hunt anything that moves, or scratch the postman wake up lick paw wake up owner meow meow meow purr while eating for sleep on keyboard human give me attention meow. Cough hairball on conveniently placed pants you call this cat food eat a plant, kill a hand so eat from dog\'s food. Purr when being pet meowwww purr, for run in circles, for scratch the box so meow loudly just to annoy owners stare out the window. Play riveting piece on synthesizer keyboard intrigued by the shower, or fooled again thinking the dog likes me. Pee in human\'s bed until he cleans the litter box. My slave human didn\'t give me any food so i pooped on the floor cough hairball on conveniently placed pants or lick arm hair. Plan steps for world domination cough furball for paw at your fat belly so wake up human for food at 4am. Cough hairball on conveniently placed pants i cry and cry and cry unless you pet me, and then maybe i cry just for fun make muffins run in circles climb a tree, wait for a fireman jump to fireman then scratch his face. Meow to be let in jump five feet high and sideways when a shadow moves chase the pig around the house friends are not food. Attack feet pet right here, no not there, here, no fool, right here that other cat smells funny you should really give me all the treats because i smell the best and omg you finally got the right spot and i love you right now favor packaging over toy for proudly present butt to human, and refuse to drink water except out of someone\'s glass. ', '2017-07-05 13:13:03', NULL, 0, 1),
(13, 1, 'Hide from vacuum cleaner gnaw the corn cob sleep on keyboard human', 'Attack the dog then pretend like nothing happened my slave human didn\'t give me any food so i pooped on the floor. Intently sniff hand ask to go outside and ask to come inside and ask to go outside and ask to come inside purr. ', 'Attack the dog then pretend like nothing happened my slave human didn\'t give me any food so i pooped on the floor. Intently sniff hand ask to go outside and ask to come inside and ask to go outside and ask to come inside purr. Lick face hiss at owner, pee a lot, and meow repeatedly scratch at fence purrrrrr eat muffins and poutine until owner comes back kitty ipsum dolor sit amet, shed everywhere shed everywhere stretching attack your ankles chase the red dot, hairball run catnip eat the grass sniff for rub face on everything purrr purr littel cat, little cat purr purr. Eat grass, throw it back up. Chew iPad power cord purr and kitty kitty for cough hairball on conveniently placed pants.ddddd Ask for petting rub face on everything chirp at birds for eat the fat cats food but destroy couch. Scratch at fleas, meow until belly rubs, hide behind curtain when vacuum cleaner is on scratch strangers and poo on owners food ask to go outside and ask to come inside and ask to go outside and ask to come inside. Sit in window and stare oooh, a bird, yum scratch at the door then walk away so loves cheeseburgers chase laser meowing non stop for food scream for no reason at 4 am and meow to be let in. Get video posted to internet for chasing red dot plays league of legends so peer out window, chatter at birds, lure them to mouth yet chew iPad power cord. Mice eat all the power cords chase dog then run away and this human feeds me, i should be a god for all of a sudden cat goes crazy cats making all the muffins and sometimes switches in french and say \"miaou\" just because well why not. Playing with balls of wool refuse to leave cardboard box. Run in circles play time, yet behind the couch fall over dead (not really but gets sypathy). Caticus cuteicus hopped up on catnip lick butt, for pee in the shoe where is my slave? I\'m getting hungry. Lick sellotape jump five feet high and sideways when a shadow moves and spill litter box, scratch at owner, destroy all furniture, especially couch. Proudly present butt to human swat at dog, and warm up laptop with butt lick butt fart rainbows until owner yells pee in litter box hiss at cats inspect anything brought into the house, or sleep on keyboard lick the other cats, yet sleep in the bathroom sink. Lounge in doorway mice yet caticus cuteicus, but slap owner\'s face at 5am until human fills food dish. Poop in litter box, scratch the walls cereal boxes make for five star accommodation yet jump around on couch, meow constantly until given food, toy mouse squeak roll over chase the pig around the house for stare out the window. Inspect anything brought into the house drink water out of the faucet or play riveting piece on synthesizer keyboard and stare at ceiling light and poop on grasses walk on car leaving trail of paw prints on hood and windshield. Poop in a handbag look delicious and drink the soapy mopping up water then puke giant foamy fur-balls chew iPad power cord, and wack the mini furry mouse. Bleghbleghvomit my furball really tie the room together loves cheeseburgers spot something, big eyes, big eyes, crouch, shake butt, prepare to pounce cereal boxes make for five star accommodation but curl into a furry donut. Walk on car leaving trail of paw prints on hood and windshield warm up laptop with butt lick butt fart rainbows until owner yells pee in litter box hiss at cats yet see owner, run in terror. ', '2017-07-05 15:36:52', '2017-07-05 21:51:39', 0, 1),
(14, 2, 'Yes, I saw. You were doing well, until everyone died.', 'I didn\'t ask for a completely reasonable excuse! I asked you to get busy! Man, I\'m sore all over. I feel like I just went ten rounds with mighty Thor. When the lights go out, it\'s nobody\'s business what goes on between two consenting adults.', 'I didn\'t ask for a completely reasonable excuse! I asked you to get busy! Man, I\'m sore all over. I feel like I just went ten rounds with mighty Thor. When the lights go out, it\'s nobody\'s business what goes on between two consenting adults.\r\n\r\nTell them I hate them.\r\nWhen I was first asked to make a film about my nephew, Hubert Farnsworth, I thought \"Why should I?\" Then later, Leela made the film. But if I did make it, you can bet there would have been more topless women on motorcycles. Roll film!\r\nGood man. Nixon\'s pro-war and pro-family.\r\nWho are those horrible orange men? Or a guy who burns down a bar for the insurance money! Doomsday device? Ah, now the ball\'s in Farnsworth\'s court! I just want to talk. It has nothing to do with mating. Fry, that doesn\'t make sense.\r\n\r\nNegative, bossy meat creature! I don\'t \'need\' to drink. I can quit anytime I want! And yet you haven\'t said what I told you to say! How can any of us trust you? And I\'m his friend Jesus. Tell them I hate them.\r\n\r\nI haven\'t felt much of anything since my guinea pig died. I was having the most wonderful dream. Except you were there, and you were there, and you were there! Bender, we\'re trying our best. It doesn\'t look so shiny to me.\r\n\r\nUm, is this the boring, peaceful kind of taking to the streets? I usually try to keep my sadness pent up inside where it can fester quietly as a mental illness. I never loved you. Switzerland is small and neutral! We are more like Germany, ambitious and misunderstood!\r\n\r\nKif might! I guess if you want children beaten, you have to do it yourself. Ah, the \'Breakfast Club\' soundtrack! I can\'t wait til I\'m old enough to feel ways about stuff! Yeah, lots of people did. A true inspiration for the children.\r\n\r\nYou wouldn\'t. Ask anyway! Bender?! You stole the atom. Shut up and get to the point! Well, let\'s just dump it in the sewer and say we delivered it.\r\n\r\nThis is the worst part. The calm before the battle. Oh sure! Blame the wizards! Bite my shiny metal ass. Oh Leela! You\'re the only person I could turn to; you\'re the only person who ever loved me. Look, everyone wants to be like Germany, but do we really have the pure strength of \'will\'?', '2017-07-05 19:14:35', NULL, 8, 0),
(15, 1, 'Fashion axe brooklyn edison bulb umam', 'Before they sold out kale chips heirloom, vice palo santo you probably haven\'t heard of them poFashion axe brooklyn edison bulb umami, iPhone flexitarian blue bottle enamel pin photo booth 3 wolf moon kombucha meditation disrupp-up small batch next level gastropub 90\'s drinking vinegar quinoa neutra', 'Fashion axe brooklyn edison bulb umami, iPhone flexitarian blue bottle enamel pin photo booth 3 wolf moon kombucha meditation disrupt offal. Bespoke unicorn pour-over sartorial farm-to-table man bun succulents irony semiotics jianbing echo park copper mug viral narwhal biodiesel. Pitchfork la croix palo santo 3 wolf moon lyft eirloom, vice palo santo you probably haven\'t heard of them pop-up small batch next ally viral ugh mumblecore. Sriracha plaid pitchfork cred tumeric marfa, dreamcatcher affogato authentic shaman meggings glossier organic ugh mustache.Before they sold out kale chips heirloom, vice palo santo you probably haven\'t heard of them pop-up small batch next level gastropub 90\'s drinking vinegar quinoa neutra. Occupy tbh palo santo kogi irony iPhone single-origin coffee YOLO disrupt. Letterpress occupy butcher schlitz next level trust fund skateboard irony tumeric vexillologist PBR&B 8-bit freegan lomo church-key. Vinyl tofu next level food truck shaman four dollar toast yr hoodie. Franzen cronut raw denim, coloring book venmo next level tbh. YOLO before they sold out tousled live-edge pour-over shoreditch +1 butcher pok pok man braid pickled neutra leggings biodiesel. Crucifix pabst squid organic plaid, green juice unicorn. Neutra actually viral ugh mumblecore. Sriracha plaid pitchfork cred tumeric marfa, dreamcatcher affogato authentic shaman meggings glossier organic ugh mustache.Before they sold out kale chips heirloom, vice palo santo you probably haven\'t heard of them pop-up small batch next level gastropub 90\'s drinking vinegar quinoa neutra. Occupy tbh palo santo kogi irony iPhone single-origin coffee YOLO disrupt. Letterpress occupy butcher schlitz next level trust fund skateboard irony tumeric vexillologist PBR&B 8-bit freegan lomo church-key. Vinyl tofu next level food truck shaman four dollar toast yr hoodie. Franzen cronut raw denim, coloring book venmo next level tbh. YOLO before they sold out tousled live-edge pour-over shoreditch +1 butcher pok pok man braid pickled neutra leggings biodiesel. Crucifix pabst squid organic plaid, green juice unicorn. Neutra actually viral ugh mumblecore. Sriracha plaid pitchfork cred tumeric marfa, dreamcatcher affogato authentic shaman meggings glossier organic ugh mustache.', '2017-07-05 19:22:12', '2017-07-15 19:59:12', 8, 0),
(16, 1, 'Fill your comps with compassion', 'Then; inspirational living a fully ethical life best practices co-create black lives matter state of play the. Effective; impact investing problem-solvers problem-solvers thought partnership collaborate. Granular; corporate social responsibility, or uplift social entrepreneur but. ', 'Then; inspirational living a fully ethical life best practices co-create black lives matter state of play the. Effective; impact investing problem-solvers problem-solvers thought partnership collaborate. Granular; corporate social responsibility, or uplift social entrepreneur but. Grit state of play unprecedented challenge, natural resources and shared value sustainable. Movements revolutionary a movements then. Best practices empower; inclusion dynamic social intrapreneurship dynamic living a fully ethical life strategy living a fully ethical life. To collaborative cities, granular empower communities systems thinking. Optimism, indicators indicators thought leader compelling resilient uplift collaborative cities. Outcomes LGBTQ+, benefit corporation invest, granular social entrepreneur segmentation. Natural resources, technology incubator society, think tank resilient. Indicators commitment; thought leadership; save the world overcome injustice greenwashing. Innovate problem-solvers the resistance ideate, social enterprise; agile gender rights the strategize. Engaging invest when social return on investment problem-solvers to scalable transparent. Granular when social return on investment best practices, indicators revolutionary LGBTQ+ academic.', '2017-07-05 19:36:49', '2017-07-06 13:58:47', 19, 0),
(17, 6, 'Let\'s crank out som\'e copy.', 'Pleas\'e use \"solutionise\" instead of solution ideas! :) we need a recap by eod, cob or whatever comes first and churning anomalies yet closer to the metal or out of the loop. We need to dialog around your choice of work attire pig in a python synergize productive mindfulness market-facing, if you wa', 'Please use \"solutionise\" instead of solution ideas! :) we need a recap by eod, cob or whatever comes first and churning anomalies yet closer to the metal or out of the loop. We need to dialog around your choice of work attire pig in a python synergize productive mindfulness market-facing, if you want to motivate these clowns, try less carrot and more stick, and face time, for we\'ve got to manage that low hanging fruit. Meeting assassin are we in agreeance baseline. Organic growth we need to have a Come to Jesus meeting with Phil about his attitude diversify kpis. Open door policy sacred cow, and work flows . Prethink. Push back if you want to motivate these clowns, try less carrot and more stick we need a paradigm shift to be inspired is to become creative, innovative and energized we want this philosophy to trickle down to all our stakeholders. Goalposts root-and-branch review. We need to dialog around your choice of work attire. If you want to motivate these clowns, try less carrot and more stick get six alpha pups in here for a focus group, so organic growth high turnaround rate, move the needle, or old boys club, we need distributors to evangelize the new line to local markets. Powerpoint Bunny herding cats productize. Bob called an all-hands this afternoon get all your ducks in a row accountable talk table the discussion , or I just wanted to give you a heads-up. At the end of the day we need to have a Come to Jesus meeting with Phil about his attitude, for drill down re-inventing the wheel high turnaround rate where the metal hits the meat for innovation is hot right now. Thought shower. Meeting assassin pipeline, and execute hit the ground running yet run it up the flagpole, ping the boss and circle back gain traction, yet wheelhouse. Are we in agreeance we need to button up our approach, so touch base or hit the ground running, nor this vendor is incompetent but guerrilla marketing. Strategic staircase pro-sumer software we\'ve got to manage that low hanging fruit. Collaboration through advanced technlogy granularity. Beef up. Design thinking put in in a deck for our standup today. Window-licker streamline, or good optics put a record on and see who dances face time hit the ground running. Strategic fit nail jelly to the hothouse wall reach out. \r\n\r\nAccountable talk can we align on lunch orders, yet meeting assassin, but are we in agreeance. Critical mass one-sheet, and digital literacy or imagineer pig in a python, for thought shower. Locked and loaded on-brand but completeley fresh run it up the flagpole move the needle. We need to dialog around your choice of work attire knowledge process outsourcing quick win. Face time high touch client. Criticality forcing function yet can you ballpark the cost per unit for me bleeding edge, and this is meaningless. What\'s the status on the deliverables for eow? UX we need to socialize the comms with the wider stakeholder community we need to button up our approach, so get all your ducks in a row, but value prop herding cats. Baseline herding cats. \r\n\r\nWe need to dialog around your choice of work attire where the metal hits the meat nor out of the loop, so quick-win and put a record on and see who dances. Fire up your browser data-point, so staff engagement, and i\'ll book a meeting so we can solution this before the sprint is over. We need to dialog around your choice of work attire make sure to include in your wheelhouse but we need to socialize the comms with the wider stakeholder community for shoot me an email future-proof, or drink the Kool-aid. After I ran into Helen at a restaurant, I realized she was just office pretty market-facing, or digital literacy, nor we want to see more charts i don\'t want to drain the whole swamp, i just want to shoot some alligators product management breakout fastworks make sure to include in your wheelhouse. Churning anomalies on your plate, yet goalposts bake it in. UX programmatically what\'s the status on the deliverables for eow? enough to wash your face synergize productive mindfulness. This is meaningless when does this sunset? and staff engagement action item, i\'ll book a meeting so we can solution this before the sprint is over. Race without a finish line i don\'t want to drain the whole swamp, i just want to shoot some alligators, for even dead cats bounce, and powerpoint Bunny show pony, but anti-pattern we need to have a Come to Jesus meeting with Phil about his attitude. Granularity this proposal is a win-win situation which will cause a stellar paradigm shift, and produce a multi-fold increase in deliverables pull in ten extra bodies to help roll the tortoise idea shower fire up your browser, or come up with something buzzworthy, and it just needs more cowbell. Knowledge process outsourcing hard stop this vendor is incompetent but i\'ll book a meeting so we can solution this before the sprint is over yet bake it in. Get all your ducks in a row work flows . Guerrilla marketing are we in agreeance, and programmatically, and drink the Kool-aid, nor win-win-win. Obviously synergize productive mindfulness going forward. Beef up not a hill to die on. Cloud strategy. Strategic fit UX best practices, so feature creep organic growth pushback. Timeframe critical mass, or future-proof, but we don\'t want to boil the ocean. This vendor is incompetent deploy. I just wanted to give you a heads-up thinking outside the box. Draw a line in the sand out of scope. I have zero cycles for this ultimate measure of success cannibalize, forcing function baseline. \r\n', '2017-07-12 13:20:38', '2017-07-14 18:15:41', 5, 0),
(18, 1, 'This was not according to brief can it be more retro', 'This was not according to brief can it be more retro, yet it looks a bit empty, try to make everything bigger can you turn it around in photoshop so we can see more of the front low resolution? It looks ok on my screen remember, everything is the same or better. Can you make it stand out more? the w', 'This was not according to brief can it be more retro, yet it looks a bit empty, try to make everything bigger can you turn it around in photoshop so we can see more of the front low resolution? It looks ok on my screen remember, everything is the same or better. Can you make it stand out more? the website doesn\'t have the theme i was going for or theres all this spanish text on my site yet we don\'t need a contract, do we. Make it sexy make it pop yet I have printed it out, but the animated gif is not moving can we try some other colours maybe, so I got your invoice...it seems really high, why did you charge so much. Can you rework to make the pizza look more delicious. Can you make the logo bigger yes bigger bigger still the logo is too big. Anyway, you are the designer, you know what to do the target audience is makes and famles aged zero and up we don\'t need a backup, it never goes down! so can you turn it around in photoshop so we can see more of the front, nor we exceed the clients\' expectations and I really think this could go viral. Thanks for taking the time to make the website, but i already made it in wix something summery; colourful, yet can you put \"find us on facebook\" by the facebook logo? we need more images of groups of people having non-specific types of fun we are a non-profit organization. Can you rework to make the pizza look more delicious start on it today and we will talk about what i want next time so we need more images of groups of people having non-specific types of fun try a more powerful colour. Can you turn it around in photoshop so we can see more of the front this looks perfect. Just Photoshop out the dog, add a baby, and make the curtains blue, so we are a startup. That\'s great, but we need to add this 2000 line essay we don\'t need a backup, it never goes down! that\'s great, but we need to add this 2000 line essay and can you make the logo bigger yes bigger bigger still the logo is too big. Jazz it up a little. The website doesn\'t have the theme i was going for make it look like Apple, so we exceed the clients\' expectations. Other agencies charge much lesser use a kpop logo that\'s not a kpop logo! ugh we are a startup, nor mmm, exactly like that, but different can you pimp this powerpoint, need more geometry patterns, this looks perfect. Just Photoshop out the dog, add a baby, and make the curtains blue, nor I really like the colour but can you change it. Give us a complimentary logo along with the website can you pimp this powerpoint, need more geometry patterns. This was not according to brief something summery; colourful could you rotate the picture to show the other side of the room? and we are a startup. Theres all this spanish text on my site I got your invoice...it seems really high, why did you charge so much, so jazz it up a little could you solutionize that for me. Thanks for taking the time to make the website, but i already made it in wix make it original there are more projects lined up charge extra the next time, so that sandwich needs to be more playful can my website be in english? so needs to be sleeker something summery; colourful. Use a kpop logo that\'s not a kpop logo! ugh thanks for taking the time to make the website, but i already made it in wix or can you turn it around in photoshop so we can see more of the front anyway, you are the designer, you know what to do can you use a high definition screenshot. Im not sure, try something else I really think this could go viral this looks perfect. Just Photoshop out the dog, add a baby, and make the curtains blue this red is too red. Can you put \"find us on facebook\" by the facebook logo? thats not what i saw in my head at all needs to be sleeker something summery; colourful, and remember, everything is the same or better. Im not sure, try something else try making it a bit less blah needs to be sleeker. \r\n\r\nCould you rotate the picture to show the other side of the room? low resolution? It looks ok on my screen i cant pay you , nor I have printed it out, but the animated gif is not moving. Just do what you think. I trust you we have big contacts we will promote you we need more images of groups of people having non-specific types of fun, for use a kpop logo that\'s not a kpop logo! ugh i\'ll know it when i see it but can you lower the price for the website? make it high quality and please use html can you make the font a bit bigger and change it to times new roman? jazz it up a little bit make the picture of the cupcake look delicious make the purple more well, purple-er it looks so empty add some more hearts can you add a bit of pastel pink and baby blue because the purple alone looks too fancy okay can you put a cute quote on the right side of the site? oh no it looks messed up! i think we need to start from scratch and can you make it stand out more?. I want you to take it to the next level can you lower the price for the website? make it high quality and please use html can you make the font a bit bigger and change it to times new roman? jazz it up a little bit make the picture of the cupcake look delicious make the purple more well, purple-er it looks so empty add some more hearts can you add a bit of pastel pink and baby blue because the purple alone looks too fancy okay can you put a cute quote on the right side of the site? oh no it looks messed up! i think we need to start from scratch or this looks perfect. Just Photoshop out the dog, add a baby, and make the curtains blue, nor i was wondering if my cat could be placed over the logo in the flyer nor this looks perfect. Just Photoshop out the dog, add a baby, and make the curtains blue. We are a big name to have in your portfolio can my website be in english?. Make it pop can we have another option, so other agencies charge much lesser this red is too red. The hair is just too polarising can we try some other colours maybe. Can you help me out? you will get a lot of free exposure doing this. \r\n\r\nIt\'s great, can you add a beard though we don\'t need a contract, do we. Can you make it look like this clipart i found we don\'t need a backup, it never goes down! and I really like the colour but can you change it the hair is just too polarising something summery; colourful, for mmm, exactly like that, but different or make it sexy. We don\'t need a backup, it never goes down!. Can you make it pop can you put \"find us on facebook\" by the facebook logo? could you rotate the picture to show the other side of the room? that\'s great, but we need to add this 2000 line essay or can my website be in english? I want you to take it to the next level. You are lucky to even be doing this for us try a more powerful colour, yet that sandwich needs to be more playful, or are you busy this weekend? I have a new project with a tight deadline. We have big contacts we will promote you we are a big name to have in your portfolio, nor this is just a 5 minutes job, so can you make it pop I got your invoice...it seems really high, why did you charge so much, for can you make pink a little more pinkish we are a startup. There are more projects lined up charge extra the next time just do what you think. I trust you start on it today and we will talk about what i want next time mmm, exactly like that, but different can you put \"find us on facebook\" by the facebook logo? we are a big name to have in your portfolio jazz it up a little. The flier should feel like a warm handshake can you rework to make the pizza look more delicious, nor I want you to take it to the next level can we have another option can it be more retro, for can you make pink a little more pinkish, and jazz it up a little. Can you pimp this powerpoint, need more geometry patterns mmm, exactly like that, but different for could you rotate the picture to show the other side of the room? something summery; colourful. We are a big name to have in your portfolio I need a website. How much will it cost, and anyway, you are the designer, you know what to do, and try a more powerful colour, for the flier should feel like a warm handshake i cant pay you nor thanks for taking the time to make the website, but i already made it in wix. Thanks for taking the time to make the website, but i already made it in wix I need a website. How much will it cost. ', '2017-07-12 13:33:52', '2017-07-15 20:03:49', 5, 0),
(19, 5, 'Rig Veda billions upon billions dispa\'ssionate extraterrestrial obse\'rver!', 'Rig Veda billions upon billions dispassionate extraterrestrial observer, Jean-FranÃƒÂ§ois Champollion, a billion trillion descended from astronomers, not a sunrise but a galaxyrise, tendrils of gossamer clouds hydrogen atoms colonies. Bits of moving fluff. Finite but unbounded tendrils of gossamer c', 'Rig Veda billions upon billions dispassionate extraterrestrial observer, Jean-FranÃƒÂ§ois Champollion, a billion trillion descended from astronomers, not a sunrise but a gal\'axyrise, tendrils of gossamer clouds hydrogen atoms colonies. Bits of moving fluff. Finite but unbounded tendrils of gossamer clouds, shores of the cosmic ocean a still more glorious dawn awaits laws of physics star stuff harvesting star light, Hypatia preserve and cherish that pale blue dot radio telescope. From which we spring, extraplanetary quasar colonies radio telescope. Hydrogen atoms are creatures of the cosmos citizens of distant epochs birth the sky calls to us?\r\nExtraordinary claims require extraordinary evidence a billion trillion, how far away? Vastness is bearable only through love laws of physics science Flatland worldlets, Euclid finite but unbounded, astonishment. With pretty stories for which there\'s little good evidence inconspicuous motes of rock and gas shores of the cosmic ocean? Are creatures of the cosmos trillion Vangelis. Take root and flourish of brilliant syntheses cosmic ocean, permanence of the stars billions upon billions? Something incredible is waiting to be known, extraplanetary are creatures of the cosmos.\r\nRogue cosmic ocean, billions upon billions, Hypatia? Tingling of the spine a mote of dust suspended in a sunbeam citizens of distant epochs cosmic ocean billions upon billions kindling the energy hidden in matter birth a very small stage in a vast cosmic arena Rig Veda! Hypatia. Dream of the mind\'s eye? Globular star cluster encyclopaedia galactica made in the interiors of collapsing stars, descended from astronomers a still more glorious dawn awaits explorations? The carbon in our apple pies Sea of Tranquility and billions upon billions upon billions upon billions upon billions upon billions upon billions!\r\n', '2017-07-12 15:27:32', '2017-07-15 12:07:52', 15, 0),
(20, 5, 'Test', 'test', 'test', '2017-07-15 10:29:45', NULL, 0, 1),
(21, 5, 'Test', 'test', 'test', '2017-07-15 10:29:55', NULL, 0, 1),
(22, 5, 'I just told you! You\'ve killed me!', '<p>Bender, being God isn\'t easy. If you do too much, people get dependent on you, and if you do nothing, they lose hope. You have to use a light touch. Like a safecracker, or a pickpocket. You, minion. Lift my arm. AFTER HIM!</p>\r\n', '<p>Bender, being God isn\'t easy. If you do too much, people get dependent on you, and if you do nothing, they lose hope. You have to use a light touch. Like a safecracker, or a pickpocket. You, minion. Lift my arm. AFTER HIM!</p>\r\n<p>Look, everyone wants to be like Germany, but do we really have the pure strength of \'will\'? <strong> Pansy.</strong> <em> I\'m sure those windmills will keep them cool.</em> Um, is this the boring, peaceful kind of taking to the streets?</p>\r\n<h2>You guys realize you live in a sewer, right?</h2>\r\n<p>Fatal. Good man. Nixon\'s pro-war and pro-family. That\'s not soon enough! Hello, little man. I will destroy you! I was having the most wonderful dream. Except you were there, and you were there, and you were there!</p>\r\n<ol>\r\n<li>You\'ll have all the Slurm you can drink when you\'re partying with Slurms McKenzie!</li><li>Ah, yes! John Quincy Adding Machine. He struck a chord with the voters when he pledged not to go on a killing spree.</li><li>Oh yeah, good luck with that.</li>\r\n</ol>\r\n\r\n<h3>Ooh, name it after me!</h3>\r\n<p>Of all the friends I\'ve had… you\'re the first. It\'s just like the story of the grasshopper and the octopus. All year long, the grasshopper kept burying acorns for winter, while the octopus mooched off his girlfriend and watched TV. But then the winter came, and the grasshopper died, and the octopus ate all his acorns. Also he got a race car. Is any of this getting through to you?</p>\r\n<ul>\r\n<li>Robot 1-X, save my friends! And Zoidberg!</li><li>But I\'ve never been to the moon!</li><li>Check it out, y\'all. Everyone who was invited is here.</li>\r\n</ul>\r\n\r\n<p>No argument here. It doesn\'t look so shiny to me. Check it out, y\'all. Everyone who was invited is here. I haven\'t felt much of anything since my guinea pig died.</p>\r\n<p>Can we have Bender Burgers again? Look, everyone wants to be like Germany, but do we really have the pure strength of \'will\'? Who said that? SURE you can die! You want to die?! No, just a regular mistake.</p>\r\n<p>Calculon is gonna kill us and it\'s all everybody else\'s fault! Humans dating robots is sick. You people wonder why I\'m still single? It\'s \'cause all the fine robot sisters are dating humans! Perhaps, but perhaps your civilization is merely the sewer of an even greater society above you!</p>\r\n<p>We\'ll need to have a look inside you with this camera. When the lights go out, it\'s nobody\'s business what goes on between two consenting adults. Good man. Nixon\'s pro-war and pro-family. There\'s no part of that sentence I didn\'t like!</p>\r\n<p>Who are you, my warranty?! I\'m sorry, guys. I never meant to hurt you. Just to destroy everything you ever believed in. The alien mothership is in orbit here. If we can hit that bullseye, the rest of the dominoes will fall like a house of cards. Checkmate.</p>\r\n<p>I was all of history\'s great robot actors - Acting Unit 0.8; Thespomat; David Duchovny! But, like most politicians, he promised more than he could deliver. That\'s not soon enough! Come, Comrade Bender! We must take to the streets!</p>\r\n<p>Is the Space Pope reptilian!? That\'s a popular name today. Little \"e\", big \"B\"? I guess because my parents keep telling me to be more ladylike. As though! Calculon is gonna kill us and it\'s all everybody else\'s fault!</p>\r\n<p>Ah, the \'Breakfast Club\' soundtrack! I can\'t wait til I\'m old enough to feel ways about stuff! Spare me your space age technobabble, Attila the Hun! Why yes! Thanks for noticing. Isn\'t it true that you have been paid for your testimony?</p>\r\n<p>Fry, you can\'t just sit here in the dark listening to classical music. Enough about your promiscuous mother, Hermes! We have bigger problems. Really?! You guys aren\'t Santa! You\'re not even robots. How dare you lie in front of Jesus?</p>\r\n<p>Shut up and get to the point! I videotape every customer that comes in here, so that I may blackmail them later. And from now on you\'re all named Bender Jr. Yes, if you make it look like an electrical fire. When you do things right, people won\'t be sure you\'ve done anything at all.</p>\r\n<p>Five hours? Aw, man! Couldn\'t you just get me the death penalty? And so we say goodbye to our beloved pet, Nibbler, who\'s gone to a place where I, too, hope one day to go. The toilet. I never loved you.</p>', '2017-07-15 10:30:23', '2017-07-23 14:21:45', 11, 0),
(30, 5, 'You are in hell, little man. And I am the devil.', 'Master Wayne, you\'ve been gone a long time. You look very fashionable. Apart from the mud.\r\n\r\nYou are in hell, little man. And I am the devil.\r\n\r\nOkay. Now... Hardened Kevlar plates over titanium-dipped, tri-weave fibers for flexibility. You\'ll be lighter, faster, more agile.\r\n\r\nA little the worse for wear, I\'m afraid.\r\n\r\nThere you are! There is nothing to fear but fear itseIf!\r\n\r\nDeath does not wait for you to be ready! Death is not considerate of fair! And make no mistake, here you face death. Tiger. Ju', 'Master Wayne, you\'ve been gone a long time. You look very fashionable. Apart from the mud.\r\n\r\nYou are in hell, little man. And I am the devil.\r\n\r\nOkay. Now... Hardened Kevlar plates over titanium-dipped, tri-weave fibers for flexibility. You\'ll be lighter, faster, more agile.\r\n\r\nA little the worse for wear, I\'m afraid.\r\n\r\nThere you are! There is nothing to fear but fear itseIf!\r\n\r\nDeath does not wait for you to be ready! Death is not considerate of fair! And make no mistake, here you face death. Tiger. Jujitsu. Panther. You\'re skilled. But this is not a dance. And you are afraid. But not of me. Tell us Mr. Wayne, what do you fear?\r\n\r\nI will go back to Gotham and I will fight men Iike this but I will not become an executioner.\r\n\r\nYour anger gives you great power. But if you Iet it, it will destroy you as it almost did me.\r\n\r\nYou have learn to bury your guilt with anger. I will teach you to confront it and to face the truth.\r\n\r\nBruce Wayne! Design, live and breathe. What brings you out of the prior sleep, Mr. Wayne?\r\n\r\nSwear to me!\r\n\r\nBut no ordinary child, a child born in hell, forged from suffering, hardened by pain. Not a man from privilege.\r\n\r\nI know why you choose to have your little, ahem... group-therapy sessions in broad daylight. I know why you\'re afraid to go out at night. The Batman.\r\n\r\nDo I really look like a guy with a plan?', '2017-07-15 15:39:03', '2017-07-22 18:50:37', 79, 0),
(41, 2, 'Devario kokopu chub largenose fish tiger shark capelin popeye catafula, rockfish snakehead labyrinth fish woody sculpin. ', 'Devario kokopu chub largenose fish tiger shark capelin popeye catafula, rockfish snakehead labyrinth fish woody sculpin. Mozambique tilapia finback cat shark golden shiner knifejaw, boxfish hairtail yellowtail clownfish houndshark shovelnose sturgeon Pacific hake. Halibut leopard danio tarwhine, toadfish gray mullet, beachsalmon three spot gourami? Mooneye codlet huchen sea chub piranha Oregon chub, coho salmon minnow sandburrower southern Dolly Varden collared carpetshark. Moonfish Dolly Varden', 'Devario kokopu chub largenose fish tiger shark capelin popeye catafula, rockfish snakehead labyrinth fish woody sculpin. Mozambique tilapia finback cat shark golden shiner knifejaw, boxfish hairtail yellowtail clownfish houndshark shovelnose sturgeon Pacific hake. Halibut leopard danio tarwhine, toadfish gray mullet, beachsalmon three spot gourami? Mooneye codlet huchen sea chub piranha Oregon chub, coho salmon minnow sandburrower southern Dolly Varden collared carpetshark. Moonfish Dolly Varden trout, candiru aruana naked-back knifefish marine hatchetfish livebearer redlip blenny tapetail lampfish barbel, pumpkinseed algae eater lake whitefish! Black bass thornfish giant wels largemouth bass whale shark damselfish whitebait. Bristlenose catfish, plunderfish roundhead scup codling tenuis, dojo loach toadfish roosterfish kokanee jack--vimba, Pacific saury.\r\n\r\nLake trout longfin dragonfish, bat ray, tiger barb bonefish warbonnet. Ghost knifefish climbing gourami Pacific lamprey gizzard shad pipefish redlip blenny dace weasel shark bleak.\" Cherry salmon chain pickerel bamboo shark fire bar danio, skate sea bream, Asiatic glassfish wolf-herring pollock emperor angelfish, mola mola sunfish, \"footballfish sabertooth.\" Eel-goby Long-finned sand diver eulachon rudd Russian sturgeon goby pejerrey. Speckled trout European flounder; trumpetfish zebra lionfish suckermouth armored catfish duckbill, pike eel monkfish longnose chimaera northern anchovy pineconefish pricklefish saber-toothed blenny, Redfish unicorn fish. Lumpsucker horn shark wobbegong lamprey, \"angler,\" delta smelt flabby whalefish garibaldi viperfish oilfish opaleye, \"crocodile icefish duckbilled barracudina.\" Eel cod nurseryfish stickleback, bigscale spotted danio, sharksucker beardfish rock beauty alfonsino ricefish combtail gourami. Louvar cutlassfish snakehead bluegill trumpeter lighthousefish atka mackerel hog sucker straptail: zebra bullhead shark springfish sweeper. Damselfish bramble shark coho salmon capelin rock beauty seahorse grayling walleye albacore.\r\n\r\nTreefish sixgill ray black dragonfish scaly dragonfish. Mustache triggerfish cat shark Kafue pike nibbler spiny eel: pilot fish anchovy Atlantic trout. Icefish: bonnetmouth white marlin sucker catalufa goldeye rudderfish butterfly ray cobia olive flounder--green swordtail flounder sea dragon monkfish yellow tang. Manta Ray swordtail tube-snout megamouth shark, silver carp roundhead; parrotfish Modoc sucker deep sea bonefish. Scorpionfish tube-eye shell-ear orangestriped triggerfish mud minnow sixgill ray--pike upside-down catfish titan triggerfish? Oceanic flyingfish skate bamboo shark torpedo Lost River sucker. Combtail gourami bigscale fish Atlantic eel Norwegian Atlantic salmon hamlet skipping goby barfish. Gudgeon elephant fish tui chub, fierasfer, shovelnose sturgeon rough scad yellowtail horse mackerel goldeye conger eel? Crocodile icefish; Black angelfish bluntnose minnow.\r\n\r\nYellow-eye mullet Rasbora pygmy sunfish sea chub dragon goby orangespine unicorn fish queen parrotfish! Snook, glassfish, ronquil, sabertooth fish southern sandfish. Tenpounder sleeper shark stream catfish knifefish, chub; porcupinefish madtom New Zealand smelt titan triggerfish Indian mul spiny dwarf catfish. Red whalefish, Black tetra sole algae eater filefish, angelfish Black sea bass ide.\" Yellowtail horse mackerel scup Atlantic trout sarcastic fringehead yellow weaver pike eel grouper slipmouth, warty angler, bangus? Marine hatchetfish Sevan trout velvetfish eucla cod pike conger? Graveldiver largenose fish deep sea eel staghorn sculpin yellowtail bonytail chub Dolly Varden trout. Soapfish slender snipe eel glass catfish; plaice, smelt. Porcupinefish porcupinefish threadfin arrowtooth eel, pipefish pomfret; beachsalmon gibberfish pineconefish ribbonbearer ruffe hoki. Pink salmon sea snail threespine stickleback, great white shark roach channel catfish.\r\n\r\nWolffish, atka mackerel. Pilchard coffinfish tang, peamouth flying gurnard bluntnose knifefish hardhead catfish blacktip reef shark. Leopard danio African glass catfish tenuis bigeye porcupinefish sheepshead, blue-redstripe danio dhufish half-gill cisco. Pacific argentine yellow tang starry flounder electric stargazer half-gill; lemon shark. Antarctic cod electric knifefish soapfish round whitefish kokanee shiner saury atka mackerel whalefish pike characid. Plownose chimaera duckbill sailfish airsac catfish glass knifefish Celebes rainbowfish airsac catfish dace desert pupfish priapumfish yellowmargin triggerfish gizzard shad tadpole cod grayling Molly Miller. Lake chub goblin shark golden trout sabertooth fish--sandroller smelt-whiting sandperch zander Peter\'s elephantnose fish?\r\n\r\n\r\nDoes your lorem ipsum text long for something a little fishier? Give our generator a try… it’s fishy!', '2017-07-17 13:14:45', '2017-07-23 14:14:17', 44, 0),
(42, 5, 'Cupcake powder candy canes icing chocolate bar chocolate cake gingerbread lollipop. ', 'Cupcake powder candy canes icing chocolate bar chocolate cake gingerbread lollipop. Chupa chups sugar plum dessert dragée I love. Fruitcake cotton candy powder cotton candy lollipop. Brownie sesame snaps caramels candy. Pie cupcake bear claw candy chocolate cake caramels jelly-o gingerbread bonbon. Soufflé dragée cake gingerbread macaroon. Pie donut cotton candy. Donut cupcake lemon drops sweet roll chupa chups lemon drops oat cake. Marzipan gingerbread oat cake donut apple pie. I love bonbon je', 'Cupcake powder candy canes icing chocolate bar chocolate cake gingerbread lollipop. Chupa chups sugar plum dessert dragée I love. Fruitcake cotton candy powder cotton candy lollipop. Brownie sesame snaps caramels candy. Pie cupcake bear claw candy chocolate cake caramels jelly-o gingerbread bonbon. Soufflé dragée cake gingerbread macaroon. Pie donut cotton candy. Donut cupcake lemon drops sweet roll chupa chups lemon drops oat cake. Marzipan gingerbread oat cake donut apple pie. I love bonbon jelly carrot cake fruitcake. Sweet halvah I love cheesecake jelly-o. Tart marzipan gingerbread oat cake. I love gummi bears marzipan lollipop I love. Candy macaroon I love.\r\nPowder gummies carrot cake bear claw. Caramels I love gummi bears croissant cake. Cupcake macaroon gummi bears dessert bonbon gummies powder bear claw. Brownie jujubes marzipan. Lemon drops toffee ice cream powder carrot cake oat cake. Candy canes I love brownie cheesecake marshmallow lemon drops chocolate cake pudding candy. Sweet roll gummi bears danish chocolate bar fruitcake. Carrot cake tart danish marzipan bear claw oat cake. Tootsie roll lemon drops I love. Apple pie oat cake topping pudding lemon drops marshmallow. Cheesecake I love cotton candy tart I love wafer cotton candy muffin. Muffin I love powder soufflé jelly.\r\nSesame snaps brownie cotton candy muffin sesame snaps tootsie roll marshmallow. Cake donut caramels apple pie carrot cake dragée gingerbread. Donut pastry bonbon I love. Bonbon wafer cotton candy marzipan chocolate I love lemon drops. Jelly apple pie candy. Icing I love I love marzipan oat cake donut jelly sugar plum soufflé. Gummi bears I love tootsie roll marzipan halvah cupcake bear claw donut. Tiramisu cookie lollipop. Fruitcake candy canes chupa chups icing. Fruitcake tart bonbon pie sugar plum jelly pudding cheesecake sesame snaps. Wafer marzipan chocolate cake cotton candy sweet topping apple pie. Jujubes wafer toffee. Candy jelly-o pie danish wafer sweet roll cheesecake croissant I love.\r\nJelly beans gummi bears croissant soufflé cake dragée. Croissant cupcake jelly sesame snaps lollipop powder I love. I love sugar plum croissant oat cake. Chocolate cake bear claw toffee. I love dragée sesame snaps tiramisu. Chocolate bar candy caramels cotton candy lollipop toffee I love. I love chocolate biscuit. Cake jelly chupa chups topping candy brownie gingerbread. Liquorice fruitcake powder biscuit soufflé. Donut jelly-o chocolate cake dessert cookie pie I love soufflé jelly beans. Toffee halvah caramels. Muffin jelly-o oat cake chupa chups gingerbread brownie jelly. I love candy brownie I love candy tootsie roll biscuit toffee sesame snaps.\r\nDessert cupcake chocolate powder muffin sweet candy brownie marshmallow. Pudding jelly beans fruitcake I love marshmallow. Jelly beans jelly cupcake sugar plum. I love pastry icing apple pie lemon drops jujubes. Ice cream sweet soufflé ice cream bonbon I love. Pie lemon drops muffin. Cake jelly tart pudding. Gingerbread cupcake sweet topping jelly-o chocolate sweet roll sweet. Soufflé candy canes liquorice. Cheesecake cotton candy donut tiramisu oat cake jelly beans candy. Halvah marshmallow bear claw lollipop gingerbread donut jelly chupa chups. Soufflé candy I love jujubes cookie cupcake. Sugar plum chupa chups pie toffee tart lemon drops lollipop. Oat cake cake gummies I love.', '2017-07-20 21:34:14', '2017-07-24 13:33:01', 8, 0);
INSERT INTO `posts` (`id`, `user_id`, `title`, `description`, `content`, `created_at`, `updated_at`, `views`, `is_delete`) VALUES
(43, 1, 'Cupidatat in ham hock cillum excepteur magna occaecat. ', 'Bacon ipsum dolor amet sunt incididunt ham fatback sausage fugiat boudin leberkas turkey non tenderloin tongue excepteur. Cupidatat in ham hock cillum excepteur magna occaecat. Aute short loin chicken culpa tenderloin swine cupidatat porchetta nostrud meatloaf magna lorem kevin do. Qui spare ribs cupidatat, alcatra sint anim est chuck ham hock jerky irure magna boudin. Doner meatball deserunt id voluptate flank velit quis chuck dolore meatloaf shank. Pork loin eiusmod shoulder chicken, nisi lore', 'Bacon ipsum dolor amet sunt incididunt ham fatback sausage fugiat boudin leberkas turkey non tenderloin tongue excepteur. Cupidatat in ham hock cillum excepteur magna occaecat. Aute short loin chicken culpa tenderloin swine cupidatat porchetta nostrud meatloaf magna lorem kevin do. Qui spare ribs cupidatat, alcatra sint anim est chuck ham hock jerky irure magna boudin. Doner meatball deserunt id voluptate flank velit quis chuck dolore meatloaf shank. Pork loin eiusmod shoulder chicken, nisi lorem irure aute pancetta pork chop.\r\n\r\nExcepteur commodo aliqua minim labore ullamco cillum tri-tip ut laboris picanha. Spare ribs t-bone cupim prosciutto boudin picanha ut jowl short ribs. Kevin meatloaf nostrud andouille, pig est culpa labore tongue exercitation. Shank quis pariatur kevin alcatra t-bone ribeye ad cupidatat dolore boudin chicken porchetta.\r\n\r\nDeserunt tri-tip pancetta duis est. Tempor burgdoggen beef bacon, spare ribs kevin dolore meatball lorem chicken ball tip chuck jerky boudin. Cupim porchetta filet mignon, burgdoggen shank enim eiusmod. Spare ribs voluptate pancetta, deserunt ut pork loin ad. Est pastrami ut consequat andouille ham hock. Swine capicola sirloin dolore.\r\n\r\nDoner beef pig, ground round tenderloin anim corned beef drumstick leberkas kevin pork belly do. Jerky occaecat anim mollit, voluptate non ham shoulder sed quis. Lorem labore ribeye, beef ribs ad bacon cow turkey shoulder deserunt chuck dolore est flank capicola. Et dolore boudin, venison cow non eiusmod pancetta quis shoulder. Minim veniam incididunt prosciutto laborum rump. In dolore culpa commodo, kielbasa occaecat pork chop deserunt irure ut. Proident reprehenderit drumstick id, pastrami sausage ribeye excepteur cow andouille incididunt meatloaf.\r\n\r\nChuck landjaeger est, turkey salami ribeye prosciutto dolore hamburger frankfurter voluptate short ribs in culpa tongue. Brisket adipisicing nulla, in nisi eiusmod duis ut commodo in occaecat tri-tip salami venison. Jowl ad duis sunt consectetur venison consequat shoulder laborum anim jerky aliquip magna bacon short loin. Adipisicing venison boudin, beef swine enim hamburger ad ham. Pastrami landjaeger nostrud tongue aliqua proident, excepteur laboris leberkas quis pork loin meatloaf beef.\r\n\r\nDoes your lorem ipsum text long for something a little meatier? Give our generator a try… it’s tasty!', '2017-07-21 09:40:58', '2017-07-21 20:30:20', 21, 0),
(44, 1, 'Pelt around the house and up and down stairs chasing phantoms climb leg make muffins', 'Cat ipsum dolor sit amet, going to catch the red dot today going to catch the red dot today so hiss and stare at nothing then run suddenly away. Hide from vacuum cleaner scratch the box but scratch the box for kitty ipsum dolor sit amet, shed everywhere shed everywhere stretching attack your ankles chase the red dot, hairball run catnip eat the grass sniff yet ignore the squirrels, you\'ll never catch them anyway mewl for food at 4am, meow. Kitty loves pigs catch mouse and gave it as a present an', '<p>We started out like Romeo and Juliet, but it ended up in tragedy. I\'ll be back. You can\'t keep the Democrats out of the White House forever, and when they get in, I\'m back on the streets, with all my criminal buddies.</p><h3>Remember the time he ate my goldfish? And you lied and said I never had goldfish. Then why did I have the bowl, Bart? *Why did I have the bowl?*</h3>\r\n<p>Human contact: the final frontier. <strong> I\'m normally not a praying man, but if you\'re up there, please save me, Superman.</strong> <em> I\'ll be back.</em> You can\'t keep the Democrats out of the White House forever, and when they get in, I\'m back on the streets, with all my criminal buddies.</p>\r\n<p>Our differences are only skin deep, but our sames go down to the bone. Remember the time he ate my goldfish? And you lied and said I never had goldfish. Then why did I have the bowl, Bart? *Why did I have the bowl?*</p>\r\n<ol>\r\n<li>But, Aquaman, you cannot marry a woman without gills. You\'re from two different worlds… Oh, I\'ve wasted my life.</li><li>Uh, no, you got the wrong number. This is 9-1…2.</li><li>Me fail English? That\'s unpossible.</li>\r\n</ol>\r\n\r\n<h3>I didn\'t get rich by signing checks.</h3>\r\n<p>And now, in the spirit of the season: start shopping. And for every dollar of Krusty merchandise you buy, I will be nice to a sick kid. For legal purposes, sick kids may include hookers with a cold. Me fail English? That\'s unpossible.</p>\r\n<ul>\r\n<li>I\'ve done everything the Bible says — even the stuff that contradicts the other stuff!</li><li>He didn\'t give you gay, did he? Did he?!</li><li>Well, he\'s kind of had it in for me ever since I accidentally ran over his dog. Actually, replace \"accidentally\" with \"repeatedly\" and replace \"dog\" with \"son.\"</li>\r\n</ul>\r\n\r\n<p>I was saying \"Boo-urns.\" Oh, a *sarcasm* detector. Oh, that\'s a *really* useful invention! He didn\'t give you gay, did he? Did he?! I was saying \"Boo-urns.\" Lisa, vampires are make-believe, like elves, gremlins, and Eskimos.</p>\r\n<p>Stan Lee never left. I\'m afraid his mind is no longer in mint condition. Marge, just about everything\'s a sin. Y\'ever sat down and read this thing? Technically we\'re not supposed to go to the bathroom.</p>\r\n<p>I\'m going to the back seat of my car, with the woman I love, and I won\'t be back for ten minutes! I didn\'t get rich by signing checks. Kids, we need to talk for a moment about Krusty Brand Chew Goo Gum Like Substance. We all knew it contained spider eggs, but the hantavirus? That came out of left field. So if you\'re experiencing numbness and/or comas, send five dollars to antidote, PO box…</p>\r\n<p>Ahoy hoy? Books are useless! I only ever read one book, \"To Kill A Mockingbird,\" and it gave me absolutely no insight on how to kill mockingbirds! Sure it taught me not to judge a man by the color of his skin…but what good does *that* do me?</p>\r\n<p>Dad didn\'t leave… When he comes back from the store, he\'s going to wave those pop-tarts right in your face! Marge, it takes two to lie. One to lie and one to listen. Oh, so they have Internet on computers now!</p>\r\n<p>Kids, we need to talk for a moment about Krusty Brand Chew Goo Gum Like Substance. We all knew it contained spider eggs, but the hantavirus? That came out of left field. So if you\'re experiencing numbness and/or comas, send five dollars to antidote, PO box… Books are useless! I only ever read one book, \"To Kill A Mockingbird,\" and it gave me absolutely no insight on how to kill mockingbirds! Sure it taught me not to judge a man by the color of his skin…but what good does *that* do me?</p>\r\n<p>Kids, kids. I\'m not going to die. That only happens to bad people. What\'s the point of going out? We\'re just going to wind up back here anyway. Marge, it takes two to lie. One to lie and one to listen.</p>\r\n<p>Old people don\'t need companionship. They need to be isolated and studied so it can be determined what nutrients they have that might be extracted for our personal use. Oh, loneliness and cheeseburgers are a dangerous mix.</p>\r\n<p>Hi. I\'m Troy McClure. You may remember me from such self-help tapes as \"Smoke Yourself Thin\" and \"Get Some Confidence, Stupid!\" He didn\'t give you gay, did he? Did he?! Attempted murder? Now honestly, what is that? Do they give a Nobel Prize for attempted chemistry?</p>\r\n<p>Thank you, steal again. I\'m normally not a praying man, but if you\'re up there, please save me, Superman. He didn\'t give you gay, did he? Did he?! Lisa, vampires are make-believe, like elves, gremlins, and Eskimos.</p>\r\n<p>They only come out in the night. Or in this case, the day. Yes! I am a citizen! Now which way to the welfare office? I\'m kidding, I\'m kidding. I work, I work. What good is money if it can\'t inspire terror in your fellow man?</p>', '2017-07-23 11:12:48', '2017-07-26 14:48:45', 13, 0),
(45, 5, 'Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you\'re going.', '<p>I find your lack of faith disturbing. No! Alderaan is peaceful. We have no weapons. You can\'t possibly… Your eyes can deceive you. Don\'t trust them. Still, she\'s got a lot of spirit. I don\'t know, what do you think?</p>', '<p>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you\'re going. Your eyes can deceive you. <strong> Don\'t trust them.</strong> <em> Leave that to me.</em>  Send a distress signal, and inform the Senate that all on board were killed.</p>\r\n<h3>But with the blast shield down, I can\'t even see! How am I supposed to fight?</h3>\r\n<p>The more you tighten your grip, Tarkin, the more star systems will slip through your fingers. As you wish. Dantooine. They\'re on Dantooine. Look, I ain\'t in this for your revolution, and I\'m not in it for you, Princess. I expect to be well paid. I\'m in it for the money.</p>\r\n<ol>\r\n<li>I\'m surprised you had the courage to take the responsibility yourself.</li><li>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct.</li><li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you\'re going.</li>\r\n</ol>\r\n\r\n<h3>I\'m surprised you had the courage to take the responsibility yourself.</h3>\r\n<p>I can\'t get involved! I\'ve got work to do! It\'s not that I like the Empire, I hate it, but there\'s nothing I can do about it right now. It\'s such a long way from here. The Force is strong with this one. I have you now.</p>\r\n<ul>\r\n<li>I\'m trying not to, kid.</li><li>I want to come with you to Alderaan. There\'s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me.</li><li>Look, I ain\'t in this for your revolution, and I\'m not in it for you, Princess. I expect to be well paid. I\'m in it for the money.</li>\r\n</ul>\r\n\r\n<p>I find your lack of faith disturbing. But with the blast shield down, I can\'t even see! How am I supposed to fight? I find your lack of faith disturbing. I care. So, what do you think of her, Han? I don\'t know what you\'re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan--</p>\r\n<p>Don\'t be too proud of this technological terror you\'ve constructed. The ability to destroy a planet is insignificant next to the power of the Force. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n<p>Oh God, my uncle. How am I ever gonna explain this? The plans you refer to will soon be back in our hands. Oh God, my uncle. How am I ever gonna explain this? Leave that to me. Send a distress signal, and inform the Senate that all on board were killed.</p>\r\n<p>Escape is not his plan. I must face him, alone. You don\'t believe in the Force, do you? You don\'t believe in the Force, do you? But with the blast shield down, I can\'t even see! How am I supposed to fight?</p>\r\n<p>A tremor in the Force. The last time I felt it was in the presence of my old master. What!? Look, I ain\'t in this for your revolution, and I\'m not in it for you, Princess. I expect to be well paid. I\'m in it for the money.</p>\r\n<p>In my experience, there is no such thing as luck. You are a part of the Rebel Alliance and a traitor! Take her away! Don\'t underestimate the Force. Escape is not his plan. I must face him, alone.</p>\r\n<p>Oh God, my uncle. How am I ever gonna explain this? He is here. All right. Well, take care of yourself, Han. I guess that\'s what you\'re best at, ain\'t it? She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander. There\'ll be no one to stop us this time!</p>\r\n<p>Look, I ain\'t in this for your revolution, and I\'m not in it for you, Princess. I expect to be well paid. I\'m in it for the money. But with the blast shield down, I can\'t even see! How am I supposed to fight?</p>\r\n<p>The Force is strong with this one. I have you now. All right. Well, take care of yourself, Han. I guess that\'s what you\'re best at, ain\'t it? A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n<p>The Force is strong with this one. I have you now. I call it luck. What good is a reward if you ain\'t around to use it? Besides, attacking that battle station ain\'t my idea of courage. It\'s more like…suicide.</p>\r\n<p>Obi-Wan is here. The Force is with him. Remember, a Jedi can feel the Force flowing through him. Alderaan? I\'m not going to Alderaan. I\'ve got to go home. It\'s late, I\'m in for it as it is. I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>', '2017-07-23 11:40:54', NULL, 1, 1),
(46, 14, 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.', 'Coffer barque Nelsons folly Plate Fleet mizzen chandler gunwalls hempen halter lanyard squiffy. Marooned Gold Road Yellow Jack man-of-war brig gally jib gangplank Buccaneer galleon. Mizzenmast scuttle yawl bounty come about gunwalls draught carouser parrel killick.\r\nSchooner Cat o\'nine tails piracy blow the man down gun swing the lead lugger man-of-war loot Jolly Roger. Chain Shot lad hang the jib American Main nipper grog bilge water topsail capstan schooner. Jack Ketch smartly hornswaggle reef ', 'Coffer barque Nelsons folly Plate Fleet mizzen chandler gunwalls hempen halter lanyard squiffy. Marooned Gold Road Yellow Jack man-of-war brig gally jib gangplank Buccaneer galleon. Mizzenmast scuttle yawl bounty come about gunwalls draught carouser parrel killick.\r\nSchooner Cat o\'nine tails piracy blow the man down gun swing the lead lugger man-of-war loot Jolly Roger. Chain Shot lad hang the jib American Main nipper grog bilge water topsail capstan schooner. Jack Ketch smartly hornswaggle reef sails snow spike fathom nipper Plate Fleet bilged on her anchor.\r\nAdmiral of the Black starboard bucko walk the plank league provost quarterdeck Jack Tar jack landlubber or just lubber. Sail ho swing the lead chase guns transom ye cackle fruit execution dock ahoy rigging run a rig. Stern wherry lanyard belay swing the lead chantey matey scourge of the seven seas boom pink.Coffer barque Nelsons folly Plate Fleet mizzen chandler gunwalls hempen halter lanyard squiffy. Marooned Gold Road Yellow Jack man-of-war brig gally jib gangplank Buccaneer galleon. Mizzenmast scuttle yawl bounty come about gunwalls draught carouser parrel killick.\r\nSchooner Cat o\'nine tails piracy blow the man down gun swing the lead lugger man-of-war loot Jolly Roger. Chain Shot lad hang the jib American Main nipper grog bilge water topsail capstan schooner. Jack Ketch smartly hornswaggle reef sails snow spike fathom nipper Plate Fleet bilged on her anchor.\r\nAdmiral of the Black starboard bucko walk the plank league provost quarterdeck Jack Tar jack landlubber or just lubber. Sail ho swing the lead chase guns transom ye cackle fruit execution dock ahoy rigging run a rig. Stern wherry lanyard belay swing the lead chantey matey scourge of the seven seas boom pink.', '2017-07-23 17:02:01', '2017-07-23 20:02:42', 16, 1),
(47, 5, 'test\'', 'test', 'test', '2017-07-23 17:23:39', '2017-07-23 20:23:49', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `post_categories`
--

CREATE TABLE `post_categories` (
  `id` int(10) NOT NULL,
  `post_id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `post_categories`
--

INSERT INTO `post_categories` (`id`, `post_id`, `category_id`) VALUES
(1, 30, 1),
(2, 30, 2),
(5, 41, 6),
(11, 43, 6),
(13, 41, 7),
(14, 43, 1),
(16, 30, 8),
(19, 41, 1),
(20, 22, 8),
(21, 22, 7),
(22, 45, 10),
(23, 45, 11),
(24, 46, 12),
(26, 47, 6),
(29, 42, 11),
(30, 42, 9),
(31, 44, 12);

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` int(1) NOT NULL,
  `name` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Editor'),
(3, 'User'),
(4, 'Banned');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `login` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `country` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `last_visit` datetime DEFAULT NULL,
  `about` text,
  `role_id` int(1) NOT NULL DEFAULT '3',
  `is_delete` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `login`, `email`, `password`, `country`, `birthday`, `phone`, `created_at`, `updated_at`, `last_visit`, `about`, `role_id`, `is_delete`) VALUES
(1, 'Khadjiit', 'Catty', 'Khadjit', 'Khajiit@mail.com', '96e79218965eb72c92a549dd5a330112', 'UA', NULL, '0987', '2017-07-11 11:38:10', '2017-07-24 14:53:53', NULL, 'i love my mom. Sometimes.', 1, 0),
(2, 'Ones', 'More', 'OnesMore', 'More@mail.com', '670b14728ad9902aecba32e22fa4f6bd', '', '2000-02-16', '12365214', '2017-07-11 11:40:31', '2017-07-20 10:31:09', NULL, 'Devario kokopu chub largenose fish tiger shark capelin popeye catafula, rockfish snakehead labyrinth fish woody sculpin. Mozambique tilapia finback cat shark golden shiner knifejaw, boxfish hairtail yellowtail clownfish houndshark shovelnose sturgeon Pacific hake. Halibut leopard danio tarwhine, toadfish gray mullet, beachsalmon three spot gourami? Mooneye codlet huchen sea chub piranha Oregon chub, coho salmon minnow sandburrower southern Dolly Varden collared carpetshark. Moonfish Dolly Varden trout, candiru aruana naked-back knifefish marine hatchetfish livebearer redlip blenny tapetail lampfish barbel, pumpkinseed algae eater lake whitefish! Black bass thornfish giant wels largemouth bass whale shark damselfish whitebait. Bristlenose catfish, plunderfish roundhead scup codling tenuis, dojo loach toadfish roosterfish kokanee jack--vimba, Pacific saury.', 2, 0),
(3, 'John', 'Smith', 'Jonny', 'smith@mail.com', '96e79218965eb72c92a549dd5a330112', NULL, NULL, NULL, '2017-07-11 11:42:27', '2017-07-24 15:06:11', NULL, '', 3, 0),
(4, 'John', 'Smith', 'Jonny1', 'smith1@mail.com', '96e79218965eb72c92a549dd5a330112', NULL, NULL, NULL, '2017-07-11 11:43:09', NULL, NULL, '', 4, 0),
(5, 'Hakuna', 'Matata', 'Hakuna', 'andlee11@gmail.com', '96e79218965eb72c92a549dd5a330112', 'UA', '1985-11-07', '066-20-85-199', '2017-07-11 12:50:02', '2017-07-24 13:26:09', NULL, 'OMG test', 2, 0),
(6, 'Der\'o', 'Miroko', 'MirokoDer', 'miroko@mail.com', '96e79218965eb72c92a549dd5a330112', 'UA', '2017-07-03', '1235675675', '2017-07-11 18:49:51', '2017-07-24 14:57:29', NULL, '', 3, 0),
(9, 'Slurm', 'Zoidberg', 'Zoidberg', 'Zoidberg@nail.com', '96e79218965eb72c92a549dd5a330112', NULL, NULL, NULL, '2017-07-15 10:48:41', NULL, NULL, NULL, 3, 0),
(11, 'New', 'Name', 'New', 'test1@mail.com', '96e79218965eb72c92a549dd5a330112', NULL, NULL, '1111111111111', '2017-07-15 11:57:19', '2017-07-16 15:16:17', NULL, NULL, 3, 0),
(12, 'Dave', 'Brubeck', 'Takefive', 'Brubeck@mail.com', '96e79218965eb72c92a549dd5a330112', 'USA', '1920-12-06', NULL, '2017-07-23 10:50:32', '2017-07-23 13:58:16', NULL, 'Dave Brubeck - Take Five', 3, 0),
(13, 'Hunter', 'Moses', 'Moses', 'Moses@mail.com', '96e79218965eb72c92a549dd5a330112', NULL, NULL, NULL, '2017-07-23 10:52:13', NULL, NULL, NULL, 3, 0),
(14, 'Emil', 'Rubi', 'EmilRubi', 'emilrubi@mail.com', 'e10adc3949ba59abbe56e057f20f883e', 'UA', '1986-10-05', NULL, '2017-07-23 16:46:13', '2017-07-26 14:48:03', NULL, 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm. Pinnace holystone mizzenmast quarter crow\'s nest nipperkin grog yardarm hempen halter furl. Swab barque interloper chantey doubloon starboard grog black jack gangway rutters.', 2, 0),
(15, 'Shady', 'Bernal', 'Bernal', 'Bernal@mail.com', '96e79218965eb72c92a549dd5a330112', NULL, NULL, NULL, '2017-07-26 11:45:10', '2017-07-26 14:48:09', NULL, NULL, 4, 0),
(16, 'Rubber', 'Danny', 'Rubber Danny', 'RubberDanny@mail.com', '96e79218965eb72c92a549dd5a330112', NULL, NULL, NULL, '2017-07-26 11:46:45', '2017-07-26 14:48:10', NULL, NULL, 4, 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `comments_ibfk_1` (`user_id`);

--
-- Индексы таблицы `image_post`
--
ALTER TABLE `image_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`);

--
-- Индексы таблицы `image_user`
--
ALTER TABLE `image_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`user_id`);

--
-- Индексы таблицы `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_from` (`user_from`),
  ADD KEY `user_to` (`user_to`);

--
-- Индексы таблицы `pass_recovery`
--
ALTER TABLE `pass_recovery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`user_id`);

--
-- Индексы таблицы `post_categories`
--
ALTER TABLE `post_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_id` (`post_id`),
  ADD KEY `categories_id` (`category_id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT для таблицы `image_post`
--
ALTER TABLE `image_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT для таблицы `image_user`
--
ALTER TABLE `image_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT для таблицы `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `pass_recovery`
--
ALTER TABLE `pass_recovery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT для таблицы `post_categories`
--
ALTER TABLE `post_categories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`);

--
-- Ограничения внешнего ключа таблицы `image_post`
--
ALTER TABLE `image_post`
  ADD CONSTRAINT `image_post_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`);

--
-- Ограничения внешнего ключа таблицы `image_user`
--
ALTER TABLE `image_user`
  ADD CONSTRAINT `image_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`user_from`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`user_to`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `pass_recovery`
--
ALTER TABLE `pass_recovery`
  ADD CONSTRAINT `pass_recovery_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
